DELETE FROM userdepartment;# 5 rows affected.
DELETE FROM user;# 10 rows affected.
DELETE FROM courses;# 8 rows affected.
DELETE FROM departments;# 3 rows affected.
DELETE FROM pictures;# MySQL returned an empty result set (i.e. zero rows).
DELETE FROM PROCESS ;# 2 rows affected.
DELETE FROM reservable;# 10 rows affected.
DELETE FROM reservation;# MySQL returned an empty result set (i.e. zero rows).
DELETE FROM reserved;

drop table userdepartment;# 5 rows affected.
drop table user;# 10 rows affected.
drop table courses;# 8 rows affected.
drop table departments;# 3 rows affected.
drop table pictures;# MySQL returned an empty result set (i.e. zero rows).
drop table reserved;
drop table reservable;# 10 rows affected.
drop table reservation;# MySQL returned an empty result set (i.e. zero rows).
drop table process ;# 2 rows affected.
drop table migrations;
drop table password_resets;

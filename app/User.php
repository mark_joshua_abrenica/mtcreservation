<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'lastname', 'firstname', 'middlename', 'accountType', 'course_id', 'department_id', 'active'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    public function getNotifications(){
        $count = reservation::where('user_id', $this->id)->where('notify', true)->count();

        return ($count > 0) ? "<div style = 'text-align:center;margin-top:-10px;width:32px;background-color:red;color:white;float:right;' id = 'notification'>$count</div>" : "<div id = 'notification'></div>";
    }
     public function getNotifications2(){
        $count = reservation::where('user_id', $this->id)->where('notify', true)->count();

        return ($count > 0) ? "<div style = 'text-align:center;margin-top:-10px;width:32px;background-color:red;color:white;float:right;' id = 'notification2'>$count</div>" : "<div id = 'notification2'></div>";
    }
    public function checkIfMyDepartment($department_id){
        // $output = userdepartment::where('user_id', $this->id)->where('department_id', $department_id)->get();
        // if(count($output) > 0 ){
        //     return true;
        // } else{
        //     return false;
        // }
        if($department_id == $this->department_id){
            return true;
        }
        else{return false;}
    }
    public function checkIfMyCourse($course_id){
        // $count = userdepartment::where('user_id', $this->id)->where('course_id', $course_id)->count();
        // if($count > 0 ){
        //     return true;
        // } else{
        //     return false;
        // }
        if($course_id == $this->course_id){
            return true;
        }
        else{return false;}
    }
    public function getCourses(){
         $assignedCourses = userdepartment::where('user_id', $this->id)->whereNotNull('course_id')->get();
         $inCourses = [];
         foreach($assignedCourses as $course){
            array_push($inCourses, $course->course_id);
         }
         $courses = courses::whereIn('id', $inCourses)->get();

         return ($courses) ? $courses : ""; 
    }
    //for non students
    public function getDepartments(){
         $assignedDepartments = userdepartment::where('user_id', $this->id)->get();
         $inDepartment = [];
         foreach($assignedDepartments as $dept){
            array_push($inDepartment, $dept->department_id);
         }
         $departments = departments::whereIn('id', $inDepartment)->get();

         return $departments;
    }
    public function getAdminDepartment(){
        $department = departments::find($this->department_id);
        if($department){
            return $department->department;
        } else{
            return "";
        }
    }
    public function getAdminCourse(){
        $course = courses::find($this->course_id);
        if($course){
            return $course->course;
        } else{
            return "";
        }
    }
    
    public function getCourse(){
        $course = courses::find($this->course_id);
        if($course){
            return $course->course;
        }
        else {
            return "";
        }
    }
}

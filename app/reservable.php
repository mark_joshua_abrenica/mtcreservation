<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reservable extends Model
{
    //
    protected $table = 'reservable';
    protected $fillable = ['type', 'venueOrEquipment','details','picture','serial','availableQuantity', 'exemptedvpaa'];
    public function getAvailableQuantity($dateTimeStart, $dateTimeEnd){
    	$conflictingReservation = reserved::where('reservable_id',$this->id)->where('dateTimeStart', '<=', $dateTimeEnd)->where('dateTimeStart', '>=', $dateTimeStart)->where('status','!=', 'closed')->where('status','!=', 'dissapproved')->get();
		$conflictingReservation2 = reserved::where('reservable_id',$this->id)->where('dateTimeEnd', '<=', $dateTimeEnd)->where("dateTimeEnd", ">=", $dateTimeStart)->where('status','!=', 'closed')->where('status','!=', 'dissapproved')->get();
		$sum = 0;
		$merge = $conflictingReservation->merge($conflictingReservation2);
		$mergeNoDuplicate = $merge->unique();
		$sum = $mergeNoDuplicate->sum('quantity');
		return $this->availableQuantity - $sum;

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class approvallog extends Model
{
    //
    protected $table = "approvallog";
    protected $fillable = ['reservation_id','status','created_at', 'updated_at'];
    public function setStatusApproved($status, $process){
    	switch($status){
                case "pending Program Head":
                $this->status = "pending Program Head";

                break;
                // case "Dean":
                // $this->setStatus("pending ITD");

                // break;

                case "pending Dean":
                    $this->status = "Endorsed by Program Head";
                
                break;
                case "pending VPAA":
                $this->status = "Endorsed by Dean";

                break;
                case "pending admin":
                if($process == 1){

                	$this->status = "Endorsed by VPAA";
                }
                else{
                	$this->status = "Endorsed by Dean";
                }
                break;
                case "approved":
                $user = Auth::user()->accountType;
                	$this->status = "Approved by $user";
                break;
            }
            $this->created_at =date("Y-m-d H:i:s");

    }
    public function setStatusDisapproved($disapprovedBy	){
    	$this->status = $disapprovedBy;

            $this->created_at = date("Y-m-d H:i:s");
    }
}
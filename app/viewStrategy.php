<?php


namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;
class viewStrategy{
	public static function courses($current = ""){
		if(Auth::check()){
			if(Auth::user()->accountType == "normal"){
				$output = "<select required name = 'course'>";
				$output .= "<option value = ''>Select course</option>";
				$courses = courses::all();
				$selected = "";

				foreach($courses as $course){
				
					$selected = (Auth::user()->course_id == $course->id ) ? "selected" : "";

				$output .= "<option $selected value = '$course->id'>$course->course</option>";
				}
				$output .= "</select>";
			} else{

				//not yet implemented
				//non-student account who have one or many assigned courses
				$output = "";
			}	
		} else{
		$output = "<select required name = 'course'>";
				$output .= "<option value = ''>Select course</option>";
				$courses = courses::all();
				$selected = "";

				foreach($courses as $course){
				

				$output .= "<option value = '$course->id'>$course->course</option>";
				}
				$output .= "</select>";
		}
		
		return $output;
	}

	public static function departments($user_id){

		$output = "<form action = '' method = 'post'>";
		$notIn = [];
		foreach(Auth::user()->getDepartments() as $department){
			array_push($notIn, $department->id);
		}
		$output .= "<select name = 'selectdepartment' placeholder = 'select to add department'>";
		$departments = departments::whereNotIn('id', $notIn)->get();
		foreach($departments as $department){
			$output .= "<option value = $department->id>$department->department_desc</option>";
		}
		$output .= "</select>";
		$output .= "<input type = 'submit' name = 'addDepartment' value = 'add department'/>";
		$output .= "</form>";
		return $output;
	}
}
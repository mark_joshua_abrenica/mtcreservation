<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\reservation;
class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $modelArray = reservation::all()->toArray();
        return response()->json($modelArray);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        
        $reservation = reservation::create(
            [
            'attendants' => $request->attendants,
            'user_id' => $request->user_id,
            'dateTimeStart' => $request->dateTimeStart,
            'dateTimeEnd' => $request->dateTimeEnd,
            'status' => $request->status,
            'purpose' => $request->purpose,
            'venue' => $request->venue,
            'event' => $request->event,
            'notify' => $request->notify,
            'dateStart' => $request->dateStart,
            'contactnumber' => $request->contactnumber,
            'process_id' => $request->process_id,

            ]
            );
       

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $arrayModel = reservation::find($id)->toArray();

        return response()->json($arrayModel);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
       
        $reservation = reservation::find($id);

        $varAttendants = ($request->has('attendants')) ? $request->attendants : $reservation->attendants;
        $varUserId = ($request->has('user_id')) ? $request->user_id : $reservation->user_id;
        $varDateTimeStart = ($request->has('dateTimeStart')) ? $request->dateTimeStart : $reservation->dateTimeStart;
        $varDateTimeEnd = ($request->has('dateTimeEnd')) ? $request->dateTimeEnd : $reservation->dateTimeEnd;
        $varStatus = ($request->has('status')) ? $request->status : $reservation->status;
        $varPurpose = ($request->has('purpose')) ? $request->purpose : $reservation->purpose;
        $varVenue = ($request->has('venue')) ? $request->venue : $reservation->venue;
        $varEvent = ($request->has('event')) ? $request->event : $reservation->event;
        $varNotify = ($request->has('notify')) ? $request->notify : $reservation->notify;
        $varDateStart = ($request->has('dateStart')) ? $request->dateStart : $reservation->dateStart;
        $varRemarks = ($request->has('remarks')) ? $request->remarks : $reservation->remarks;
        $varContactNumber = ($request->has('contactnumber')) ? $request->contactnumber : $reservation->contactnumber;
        $varProcesId = ($request->has('process_id')) ? $request->process_id : $reservation->process_id;
        
        $reservation->attendants = $varAttendants;
        $reservation->user_id = $varUserId;
        $reservation->dateTimeStart = $varDateTimeStart;
        $reservation->dateTimeEnd = $varDateTimeEnd;
        $reservation->status = $varStatus;
        $reservation->purpose = $varPurpose;
        $reservation->venue = $varVenue;
        $reservation->event = $varEvent;
        $reservation->notify = $varNotify;
        $reservation->dateStart = $varDateStart;
        $reservation->remarks = $varRemarks;
        $reservation->contactnumber = $varContactNumber;
        $reservation->process_id = $varProcesId;
        
        $reservation->save();



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
       reservation::destroy($id);
    }
}

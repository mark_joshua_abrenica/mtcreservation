<?php
namespace App\Http\Controllers;
use Illuminate\View\View;
use App\reservable;
use Illuminate\Http\Request;
use App\reservation;
use Auth;
use DB;
use App\reserved;
use App\courses;
use App\departments;
use App\User;
use App\approvallog;
use DateTime;
class HomeController extends Controller{
public function getNewcourse(){
	$data['departments'] = departments::all();
		return view('newcourse', $data);
	}
	public function postNewcourse(Request $request){
		$course = new courses();
		$course->course = $request->coursecode;
		$course->course_desc= $request->coursename;
		$course->department_id = $request->department;
		$course->save();
		return redirect('/course/'. $course->id)->with('affirm', 'New course created successfully!');
	}
	public function postcourse(Request $request, $id){
		//save

		$course = courses::find($id);
		if(isset($_POST['btnSave'])){
			$course->course= $request->coursecode;
			$course->course_desc= $request->coursename;
			$course->department_id = $request->department;
			$course->save();
			return redirect('/course/'. $id)->with('affirm', 'Saved successfull!');
		}

	}
	public function getcourse($id){
		$data['course'] = courses::find($id);
		$data['departments'] = departments::all();
		return view('course', $data);
	}
/////////////

	public function getNewDepartment(){
		return view('newdepartment');
	}
	public function postNewDepartment(Request $request){
		$department = new Departments();
		$department->department = $request->departmentcode;
		$department->department_desc= $request->departmentname;
		$department->save();
		return redirect('/department/'. $department->id)->with('affirm', 'New department created successfully!');
	}
	public function postDepartment(Request $request, $id){
		//save

		$department = departments::find($id);
		if(isset($_POST['btnSave'])){
			$department->department= $request->departmentcode;
			$department->department_desc= $request->departmentname;
			$department->save();
			return redirect('/department/'. $id)->with('affirm', 'Saved successfull!');
		}

	}
	public function getDepartment($id){
		$data['department'] = departments::find($id);
		return view('department', $data);
	}
	public function postSystemMaintenance(Request $request){
		if(isset($_POST['btnDepartmentEdit'])){
			return redirect('/department/' . $request->department_id);
		}
		if(isset($_POST['btnCourseEdit'])){
			return redirect('/course/' . $request->course_id);
		}
		
		if(isset($_POST['btnCoursesEdit'])){
			return redirect('/course/' . $request->course_id);
		}
		
		if(isset($_POST['btnDepartmentDelete'])){
			$department = departments::find($request->department_id);
			$department->delete();
			return redirect('/systemMaintenance')->with('affirm', 'Department "'.$department->department.'" Deleted successfull!');
		}
		if(isset($_POST['btnCourseDelete'])){
			$course = courses::find($request->course_id);
			$course->delete();
			return redirect('/systemMaintenance')->with('affirm', 'Course "'.$course->course.'" Deleted successfull!');
		}

		return redirect('/systemMaintenance');
	}
	public function getSystemMaintenance(Request $request){
		$data['departments'] = departments::all();
		$data['courses'] = courses::all();
		return view('SystemMaintenance', $data);
	}
	public function postActivate(Request $request){
		$activated = "";
		$user = User::find($request->user_id);
		if(isset($_POST['btnActivate'])){
			$user->active = true;
			$activated = 'activated';
			
		}
		if(isset($_POST['btnDeactivate'])){
			$user->active = false;
			$activated = 'deactivated';

		}
		$user->save();
		return redirect('/userMaintenance')->with('affirm', 'user successfully ' . $activated);
	}
	public function postClearArchivedReservation(Request $request){
		$archived = reservation::where('status', 'approved')->orWhere('status', 'disapproved')->delete();
		return redirect('/archivedreservations')->with('affirm', 'Archived reservations successfully cleared');
	}
	public function postDeleteAccount($id){
		$user = User::find($id);
		$user->forceDelete();
		return redirect('/userMaintenance')->with('affirm', 'User successfully deleted!');
	}
	public function postAddAccount(Request $request){
		if($request->password != $request->password_confirmation){

		$request->session()->flash('error', 'Passwords must match');
		$data['courses'] = courses::all();
		$data['departments'] = departments::all();
			return view('/addAccount', $data);
		}
		$user = user::create(
			
			[
			 'name' => $request->name,
			 'email' => $request->email,
			 'password' => bcrypt($request->password),
			 'lastname' => $request->lastname,
			 'firstname' => $request->firstname,
			 'middlename' => $request->middlename,
			 'accountType' => $request->accountType
			 
			 ]
			);	
			if($request->course != "") 
			{
				$user->course_id = $request->course;
			}
			if($request->department != ""){
				$user->department_id = $request->department;
			}
			$user->save();

		return redirect('/addAccount')->with('affirm', 'User successfully registered!');
	}
	public function getAddAccount(){

		$data['courses'] = courses::all();
		$data['departments'] = departments::all();
		return view('addAccount', $data);
	}
	public function postEditAccount($user_id, Request $request){
		if(Auth::check())
		{
			if(Auth::user()->accountType == "normal"){
				return redirect("/");
			}
		} else{
			return redirect("/");
		}
		$user = User::find($user_id);
		//edit user
		$varDepartment_id =  ($request->department != "") ? $request->department : null;
		$varCourse_id = ($request->course != "") ? $request->course : null;
		

		$user->department_id = $varDepartment_id;
		$user->course_id = $varCourse_id;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->lastname = $request->lastname;
		$user->firstname = $request->firstname;
		$user->middlename = $request->middlename;
		$user->accountType = $request->accountType;


		$user->save();
		return redirect('/editAccount/' . $user->id)->with('affirm','User successfully updated!');

	}
	public function getEditAccount($user_id){
		if(Auth::check())
		{
			if(Auth::user()->accountType == "normal"){
				return redirect("/");
			}
		} else{
			return redirect("/");
		}
		$data['user'] = User::find($user_id);
		$data['courses'] = courses::all();
		$data['departments'] = departments::all();
		return view('editAccount', $data);
	}

	public function getUserMaintenance(){
		$data['users'] = User::all();
		return view('UserMaintenance', $data);
	}
	public function postRegisterUser(Request $request){
		$encryptedPassword = bcrypt(urldecode($request->password));
		$user = User::create(
			['name' => urldecode($request->name),
			'email' => urldecode($request->email),
			'password' => $encryptedPassword,
			'firstname' => urldecode($request->firstname),
			'lastname' => urldecode($request->lastname),
			'middlename' =>urldecode($request->middlename),
			'course_id' => $request->course

			]
			);
	}
	public function getDepartments(){
		$departments = departments::all();
		return response()->json($departments->toArray());
	}
	public function getCourses(){
		$courses = courses::all();
		return response()->json($courses->toArray());
	}
	public function getUserByUsernamePassword(Request $request){
		if(Auth::attempt(['name' => urldecode($request->username), 'password' => urldecode($request->password)])){
			return response()->json(Auth::user()->toArray());
		}
	}
	public function postApply(Request $request){
		$dateStart = $request->input('dateStart');
		$timeStart = $request->input('timeStart') . ":00";
		$dateEnd = $request->input('dateStart');
		$timeEnd = $request->input('timeEnd') . ":00";

		$dateTimeStart = $dateStart . " " . urldecode($timeStart);
		$dateTimeEnd = $dateEnd . " " . urldecode($timeEnd);
		$hasVenue = false;
		$venue = "";
		if($request->has("venue")){
			$hasVenue = true;
			$venue = $request->venue;
		}
			$reservation = reservation::create(
				[
					
					'attendants' => $request->attendants,
					'user_id' => $request->user_id,
					'dateTimeStart' => $dateTimeStart,
					'dateTimeEnd' => $dateTimeEnd,
					'status' => 'pending Program Head',
					'purpose' => $request->purpose,
					'venue' => $venue ,
					'event' => $request->event,
					'dateStart' => $dateStart,
					'contactnumber' => $request->contactnumber
				]
			);
			
			$hasEquipment = false;
			$hasService = false;

			$reservables = reservable::all();
			$reservedItemsString = $request->reservedItems;

			$reservedItemsArray = explode(",", $reservedItemsString);

			foreach($reservables as $reservable){
				
				    // if($_POST['$reservable->id']){
					if(in_array("$reservable->id", $reservedItemsArray)){
				   		$id = $reservable->id;
				   		
				   		if($reservable->type == "equipment"){
				   			$hasEquipment = true;
				   		}
				   		if($reservable->type == "Support Service"){
				   			$hasService = true;
				   		}
				    	$reserved = reserved::create(
						[
							'reservation_id' => $reservation->id,
							'reservable_id' => $reservable->id,
							'status' => 'pending Program Head',
							'dateTimeStart' => $dateTimeStart,
							'dateTimeEnd' => $dateTimeEnd,
							'quantity' => $_GET['quantity'.$id]
						]
						);				
					}	   
				    // }
			}
			if($hasVenue == false || $hasEquipment == false){
				$reservation->process_id = 2;
			}
			else{
				$reservation->process_id = 1;
			}
			$reservation->save();
	}
	public function getAvailableSupportService(Request $request){
		$dateTimeStart = urldecode($request->datetimestart);
		$dateTimeEnd = urldecode($request->datetimeend);
$getInSupportServices = reservable::where('type', 'Support Services')->get();
		$inSupportService = [];
		foreach($getInSupportServices as $ItemInEquipment){
			array_push($inSupportService, $ItemInEquipment->id);
		}
				$reserveableSupportServices = reservable::where('enabled', true)->where('type', 'Support Services')->get();
				$reserveableSupportServiceArray = [];
				$in = [];
				foreach($reserveableSupportServices as $reserveableSupportService){
					$available = $reserveableSupportService->getAvailableQuantity($dateTimeStart, $dateTimeEnd);
					
					if($available > 0){
						array_push($in,$reserveableSupportService->id);
					
					}else{
						
					}
				}
				$reserveableEquipmentsOutput = reservable::whereIn('id', $in)->get();

				return response()->json($reserveableEquipmentsOutput->toArray());
	}
	public function getAvailableEquipment(Request $request){
$dateTimeStart = urldecode($request->datetimestart);
		$dateTimeEnd = urldecode($request->datetimeend);
$getInEquipments = reservable::where('type', 'equipment')->get();
		$inEquipment = [];
		foreach($getInEquipments as $ItemInEquipment){
			array_push($inEquipment, $ItemInEquipment->id);
		}
				$reserveableEquipments = reservable::where('enabled', true)->where('type', 'equipment')->get();
				$reserveableEquipmentsArray = [];
				$in = [];
				foreach($reserveableEquipments as $reserveableEquipment){
					$available = $reserveableEquipment->getAvailableQuantity($dateTimeStart, $dateTimeEnd);
					
					if($available > 0){
						array_push($in,$reserveableEquipment->id);
					
					}else{
						
					}
				}
				$reserveableEquipmentsOutput = reservable::whereIn('id', $in)->get();

				return response()->json($reserveableEquipmentsOutput->toArray());
	}
	public function getAvailableQuantity(Request $request){

		$dateTimeStart = urldecode($request->datetimestart);
		$dateTimeEnd = urldecode($request->datetimeend);
		$reserveable_id = $request->reserveable_id;
		$reservable = reservable::find($reserveable_id);
    	$conflictingReservation = reserved::where('reservable_id',$reserveable_id)->where('dateTimeStart', '<=', $dateTimeEnd)->where('dateTimeStart', '>=', $dateTimeStart)->where('status','!=', 'closed')->where('status','!=', 'disapproved')->get();
		$conflictingReservation2 = reserved::where('reservable_id',$reserveable_id)->where('dateTimeEnd', '<=', $dateTimeEnd)->where("dateTimeEnd", ">=", $dateTimeStart)->where('status','!=', 'closed')->where('status','!=', 'disapproved')->get();
		$sum = 0;
		$merge = $conflictingReservation->merge($conflictingReservation2);
		$mergeNoDuplicate = $merge->unique();
		$sum = $mergeNoDuplicate->sum('quantity');
		$output = $reservable->availableQuantity - $sum;
		$outputarray = [$output];
		return response()->json($outputarray);
    }
    
	public function getReservableRooms(Request $request){
		$dateTimeStart = '';
		$dateTimeEnd = '';

		$dateTimeStart = urldecode($request->datetimestart);
		$dateTimeEnd = urldecode($request->datetimeend);
		$getInRooms = reservable::where('type', '=', 'venue')->get();
		

		$inVenue = [];
		foreach($getInRooms as $ItemInVenue){
			array_push($inVenue, $ItemInVenue->id);
		}

		$conflictingVenue = reservation::whereIn('venue',$inVenue)->where('dateTimeStart', '<=', $dateTimeEnd)->where('dateTimeStart', '>=', $dateTimeStart)->where('status','!=', 'closed')->where('status','!=', 'disapproved')->get();
		$conflictingVenue2 = reservation::whereIn('venue',$inVenue)->where('dateTimeEnd', '<=', $dateTimeEnd)->where("dateTimeEnd", ">=", $dateTimeStart)->where('status','!=', 'closed')->where('status','!=', 'disapproved')->get();
		
		$conflictingReservation = reserved::whereIn('reservable_id',$inVenue)->where('dateTimeStart', '<=', $dateTimeEnd)->where('dateTimeStart', '>=', $dateTimeStart)->where('status','!=', 'closed')->where('status','!=', 'disapproved')->get();
		$conflictingReservation2 = reserved::whereIn('reservable_id',$inVenue)->where('dateTimeEnd', '<=', $dateTimeEnd)->where("dateTimeEnd", ">=", $dateTimeStart)->where('status','!=', 'closed')->where('status','!=', 'disapproved')->get();
		
		$output["rooms"] = "<div class='gallery-bottom' id = 'populaterooms'>";
				$getNotIn = reservation::where('status', '!=', 'closed')->get();
				$notIn = [];
				
				foreach($conflictingVenue as $ItemNotIn){
					array_push($notIn, $ItemNotIn->venue);
				}
				foreach($conflictingVenue2 as $ItemNotIn){
					array_push($notIn, $ItemNotIn->venue);
				}
				$search ="";
				$reserveableRooms = reservable::where('enabled', true)->where('type', '=', 'venue')->whereNotIn('id', $notIn)->get();
				if($request->has('search')){
					$search = $request->search;

					$reserveableRooms = reservable::where('enabled', true)->where('type', '=', 'venue')->whereNotIn('id', $notIn)->where("venueOrEquipment", "LIKE", "%$search%")->get();
				}
        return response()->json($reserveableRooms->toArray());
	}
	public function getToggleNotification(Request $request){
		$reservation = reservation::find($request->reservation_id);
		$reservation->notify = $request->notify;

		$reservation->save();
		$user_id = $request->user_id;
		$count = reservation::where('user_id', $user_id)->where('notify', true)->count();
		return $request->notify;
//		 return response()->json($count);
	}
	public function getArchivedReservation(Request $request){
		$sort = "";
		$dateStart = "&dateStart=";
		if($request->has('sortdateevent')){
			if($request->sortdateevent == "asc"){
				$data['sortdateeventlink'] = "&sortdateevent=desc";
				$data['sortdateeventlabel'] = "sort by date event descending";
			} else {
				$data['sortdateeventlink'] = "&sortdateevent=asc";
				$data['sortdateeventlabel'] = "sort by date event ascending";
			}
		} else{
			//by default
			$data['sortdateeventlink'] = "&sortdateevent=asc";
			$data['sortdateeventlabel'] = "sort by date event ascending";
		}

		if($request->has('sortdaterequested')){
			if($request->sortdaterequested == "asc"){
				$data['sortdaterequestedlink'] = "&sortdaterequested=desc";
				$data['sortdaterequestedlabel'] = "sort by date requested descending";
			} else {
				$data['sortdaterequestedlink'] = "&sortdaterequested=asc";
				$data['sortdaterequestedlabel'] = "sort by date requested ascending";
			}
		} else{
			//by default
			$data['sortdaterequestedlink'] = "&sortdaterequested=asc";
			$data['sortdaterequestedlabel'] = "sort by date requested ascending";
		}
		

		if(isset($_GET['sortdateevent'])){
			$data['reservations'] = reservation::where('status','disapproved')->orWhere('status','approved')->orderBy('dateTimeStart',$request->sortdateevent);
		} 

		else if(isset($_GET['sortdaterequested'])){
			$data['reservations'] = reservation::where('status','disapproved')->orWhere('status','approved')->orderBy('created_at',$request->sortdaterequested);
		} 

		else{
			//by default
			$data['reservations'] = reservation::where('status','disapproved')->orWhere('status','approved')->orderBy('updated_at','desc');
		}


		if(isset($_GET['dateStart'])){
			$dateStart = $_GET['dateStart'];
			$data['reservations'] = $data['reservations']->where('dateStart',$request->dateStart);
			if($request->has('sortdateevent')){
				if($request->sortdateevent == "asc"){
					$data['sortdateeventlink'] = "&sortdateevent=desc&dateStart=$dateStart";
					$data['sortdateeventlabel'] = "sort by date event descending";
				} else {
					$data['sortdateeventlink'] = "&sortdateevent=asc&dateStart=$dateStart";
					$data['sortdateeventlabel'] = "sort by date event ascending";
				}
			} else{
				//by default
				$data['sortdateeventlink'] = "&sortdateevent=asc&dateStart=$dateStart";
				$data['sortdateeventlabel'] = "sort by date event ascending";
			}

			if($request->has('sortdaterequested')){
				if($request->sortdaterequested == "asc"){
					$data['sortdaterequestedlink'] = "&sortdaterequested=desc&dateStart=$dateStart";
					$data['sortdaterequestedlabel'] = "sort by date requested descending";
				} else {
					$data['sortdaterequestedlink'] = "&sortdaterequested=asc&dateStart=$dateStart";
					$data['sortdaterequestedlabel'] = "sort by date requested ascending";
				}
			} else{
				//by default
				$data['sortdaterequestedlink'] = "&sortdaterequested=asc&dateStart=$dateStart";
				$data['sortdaterequestedlabel'] = "sort by date requested ascending";
			}
			
		} 

		$data['reservations'] = $data['reservations']->get(); 
		return view("archivedReservation", $data);
	}

	public function postIndex(Request $request){
		$dateStart = $request->input('dateStart');
		$timeStart = $request->input('timeStart') . ":00";
		$dateEnd = $request->input('dateStart');
		$timeEnd = $request->input('timeEnd') . ":00";

		$dateTimeStart = $dateStart . " " . $timeStart;
		$dateTimeEnd = $dateEnd . " " . $timeEnd;
		$hasVenue = false;
		$venue = "";
		if($request->has("venue")){
			$hasVenue = true;
			$venue = $request->venue;
		}
			$reservation = reservation::create(
				[
					
					'attendants' => $request->attendants,
					'user_id' => Auth::user()->id,
					'dateTimeStart' => $dateTimeStart,
					'dateTimeEnd' => $dateTimeEnd,
					'status' => 'pending Program Head',
					'purpose' => $request->purpose,
					'venue' => $venue ,
					'event' => $request->event,
					'dateStart' => $dateStart,
					'contactnumber' => $request->contactnumber
				]
			);
			
			$hasEquipment = false;
			$hasService = false;

			$reservables = reservable::all();
			foreach($reservables as $reservable){
				if ($request->has($reservable->id)) {
				    //
				    // if($_POST['$reservable->id']){
					if($reservable)
				   		$id = $reservable->id;
				   		
				   		if($reservable->type == "equipment"){
				   			$hasEquipment = true;
				   		}
				   		if($reservable->type == "Support Service"){
				   			$hasService = true;
				   		}
				    	$reserved = reserved::create(
						[
							'reservation_id' => $reservation->id,
							'reservable_id' => $reservable->id,
							'status' => 'pending Program Head',
							'dateTimeStart' => $dateTimeStart,
							'dateTimeEnd' => $dateTimeEnd,
							'quantity' => $_POST['quantity'.$id]
						]
						);					   
				    // }
				}	

			}
			if($hasVenue == false || $hasEquipment == false){
				$reservation->process_id = 2;
			}
			else{
				$reservation->process_id = 1;
			}
			$reservation->save();
		return redirect("/myreservation")->with("affirm", "Request for reservation is now processed. Please wait for approval.");
	}
	public function getRooms(Request $request){
$dateTimeStart = '';
		$dateTimeEnd = '';

		$dateTimeStart = $request->datetimestart;
		$dateTimeEnd = $request->datetimeend;
		$getInRooms = reservable::where('type', '=', 'venue')->get();
		

		$inVenue = [];
		foreach($getInRooms as $ItemInVenue){
			array_push($inVenue, $ItemInVenue->id);
		}
		$dateTimeStartWithAllowance = new DateTime($dateTimeStart);
		date_add($dateTimeStartWithAllowance, date_interval_create_from_date_string('-2 hours'));
		$conflictingVenue = reservation::whereIn('venue',$inVenue)->where('dateTimeStart', '<=', $dateTimeEnd)->where('dateTimeStart', '>=', $dateTimeStartWithAllowance)->where('status','!=', 'closed')->where('status','!=', 'disapproved')->get();
		$conflictingVenue2 = reservation::whereIn('venue',$inVenue)->where('dateTimeEnd', '<=', $dateTimeEnd)->where("dateTimeEnd", ">=", $dateTimeStartWithAllowance)->where('status','!=', 'closed')->where('status','!=', 'disapproved')->get();
		
		$conflictingReservation = reserved::whereIn('reservable_id',$inVenue)->where('dateTimeStart', '<=', $dateTimeEnd)->where('dateTimeStart', '>=', $dateTimeStartWithAllowance)->where('status','!=', 'closed')->where('status','!=', 'disapproved')->get();
		$conflictingReservation2 = reserved::whereIn('reservable_id',$inVenue)->where('dateTimeEnd', '<=', $dateTimeEnd)->where("dateTimeEnd", ">=", $dateTimeStartWithAllowance)->where('status','!=', 'closed')->where('status','!=', 'disapproved')->get();
		
		$output["rooms"] = "<div class='gallery-bottom' id = 'populaterooms'>";
				$getNotIn = reservation::where('status', '!=', 'closed')->get();
				$notIn = [];
				
				foreach($conflictingVenue as $ItemNotIn){
					array_push($notIn, $ItemNotIn->venue);
				}
				foreach($conflictingVenue2 as $ItemNotIn){
					array_push($notIn, $ItemNotIn->venue);
				}
				$search ="";
				$reserveableRooms = reservable::where('enabled', true)->where('type', '=', 'venue')->whereNotIn('id', $notIn)->get();
				if($request->has('search')){
					$search = $request->search;

					$reserveableRooms = reservable::where('enabled', true)->where('type', '=', 'venue')->whereNotIn('id', $notIn)->where("venueOrEquipment", "LIKE", "%$search%")->get();
				}
				foreach($reserveableRooms as $reserveableRoom){

				$output["rooms"] .="<div class='col-sm-4 gallery-grid'>
					
					<div class='project-eff'>
						
						<div id='nivo-lightbox-demo'></div> 
							
							<figcaption>
								<input  id = '$reserveableRoom->id' type = 'radio' name = 'venue' value = '$reserveableRoom->id' onclick='checkvenuecapacity($reserveableRoom->availableQuantity)' />
								<label for = '$reserveableRoom->id'>$reserveableRoom->venueOrEquipment</label>
								
							</figcaption>
							
						</div>

				</div>
				";
				}
		  $output["rooms"] .= "<div class='clearfix'> </div>";
		$output["rooms"] .="</div>";





$getInEquipments = reservable::where('type', 'equipment')->get();
		$inEquipment = [];
		foreach($getInEquipments as $ItemInEquipment){
			array_push($inEquipment, $ItemInEquipment->id);
		}

		// $conflictingReservation = reserved::whereIn('reservable_id',$inEquipment)->where('dateTimeStart', '<=', $dateTimeEnd)->where('dateTimeStart', '>=', $dateTimeStart)->where('status','!=', 'disapproved')->where('status','!=', 'closed')->get();
		// $conflictingReservation2 = reserved::whereIn('reservable_id',$inEquipment)->where('dateTimeEnd', '<=', $dateTimeEnd)->where("dateTimeEnd", ">=", $dateTimeStart)->where('status','!=', 'disapproved')->where('status','!=', 'closed')->get();
		$output["equipment"] = "<div class='gallery-bottom' id = 'populaterooms'>";
				$getNotIn = reservation::where('status', '!=', 'closed')->get();
				$notIn = [];
				foreach($conflictingReservation as $ItemNotIn){
					array_push($notIn, $ItemNotIn->reservable_id);
				}
				foreach($conflictingReservation2 as $ItemNotIn){
					array_push($notIn, $ItemNotIn->reservable_id);
				}
				
				$reserveableEquipments = reservable::where('enabled', true)->where('type', 'equipment')->get();
				$reserveableEquipmentsArray = [];
				$in = [];
				foreach($reserveableEquipments as $reserveableEquipment){
					$available = $reserveableEquipment->getAvailableQuantity($dateTimeStart, $dateTimeEnd);
					
					if($available > 0){
						array_push($in,$reserveableEquipment->id);
					$output["equipment"] .="<div class='col-sm-4 gallery-grid'>
					
					<div class='project-eff'>
						
						<div id='nivo-lightbox-demo'></div>  
							   
							<figcaption>
								<input id = '$reserveableEquipment->id' type = 'checkbox' name = '$reserveableEquipment->id' value = '$reserveableEquipment->id' />
								<label for = '$reserveableEquipment->id'>$reserveableEquipment->venueOrEquipment</label>
								<input style = 'width:80px;' id = 'quantity$reserveableEquipment->id' type = 'number' max = '$available' min='1'  name = 'quantity$reserveableEquipment->id' value = '$available' />
								
							</figcaption>
							
						</div>

				</div>
				";


					}else{
						
					}
				}

		  $output["equipment"] .= "<div class='clearfix'> </div>";
		$output["equipment"] .="</div>";
				











$getInServices = reservable::where('type', 'Support Services')->get();
		$inServices = [];
		foreach($getInServices as $ItemInServices){
			array_push($inServices, $ItemInServices->id);
		}

		$conflictingServices = reserved::whereIn('reservable_id',$inEquipment)->where('dateTimeStart', '<=', $dateTimeEnd)->where('dateTimeStart', '>=', $dateTimeStart)->where('status','!=', 'disapproved')->where('status','!=', 'closed')->get();
		$conflictingServices2 = reserved::whereIn('reservable_id',$inEquipment)->where('dateTimeEnd', '<=', $dateTimeEnd)->where("dateTimeEnd", ">=", $dateTimeStart)->where('status','!=', 'disapproved')->where('status','!=', 'closed')->get();
		$output["Services"] = "<div class='gallery-bottom' id = 'populateServices'>";
				$getNotIn = reservation::where('status', '!=', 'closed')->get();
				$notIn = [];
				foreach($conflictingServices as $ItemNotIn){
					array_push($notIn, $ItemNotIn->reservable_id);
				}
				foreach($conflictingServices2 as $ItemNotIn){
					array_push($notIn, $ItemNotIn->reservable_id);
				}
				
				$reserveableServices = reservable::where('enabled', true)->where('type', 'Support Services')->get();

				foreach($reserveableServices as $reserveableService){
					$available = $reserveableService->getAvailableQuantity($dateTimeStart, $dateTimeEnd);
					if($available > 0){
					$output["Services"] .="<div class='col-sm-4 gallery-grid'>
					
					<div class='project-eff'>
						
						<div id='nivo-lightbox-demo'></div>  
							   
							<figcaption>
								<input id = '$reserveableService->id' type = 'checkbox' name = '$reserveableService->id' value = '$reserveableService->id' />
								<label for = '$reserveableService->id'>$reserveableService->venueOrEquipment</label>
								<input style = 'width:80px;' id = 'quantity$reserveableService->id' type = 'number' max = '$available' min = '1' name = 'quantity$reserveableService->id' value = '$available' />
								
							</figcaption>
							
						</div>

				</div>
				";


					}else{
						
					}
				}
		  $output["Services"] .= "<div class='clearfix'> </div>";
		$output["Services"] .="</div>";
				
		 return response()->json( $output);
	}
	public function getNew(){
		if(Auth::user()->accountType == "admin"){

			$data = [];
			return view('new',$data);
		}
		//else
		return redirect("/");
	}
	public function postNew(Request $request){
		if(Auth::user()->accountType == "admin"){
			
			$exemptedvpaa_bool = ($request->exemptedvpaa == true) ? 1: 0;
			$reservable = reservable::create(
				[
					'type' => $request->type,
					'venueOrEquipment' => $request->venueOrEquipment,
					'details' => $request->details,
					'serial' => $request->serial,
					'exemptedvpaa' => $exemptedvpaa_bool,
					'availableQuantity' => $request->availableQuantity,
					''
				]
				);
			return redirect('/venuesEquipment')->with('affirm', 'added new item successfully');
		}
		//else 
		return redirect("/");
	}
	public function postMyAccount(Request $request){
		$data = [];
		if(isset($_POST['btnSave'])){
			$user = Auth::user();
			$user->name = $request->name;
			$user->lastname = $request->lastname;
			$user->firstname = $request->firstname;
			$user->middlename = $request->middlename;
			$user->email = $request->email;
			if(Auth::user()->accountType == "normal"){
				$user->course_id = $request->course;
			}
			else{
				//not yet implemented to have graphical user interface for non-student accounts management (except basic accounts info)
			}
			$user->save();
			return redirect("/myaccount")->with('affirm', 'account successfully updated');
	
		}
		if(isset($_POST['saveDepartments'])){
			//todo: do save departments for non student account
		}
	} 
	public function getMyAccount(){
		$data = [];
		return view("myaccount", $data);
	} 
	public function postEdit(Request $request){
		$reservable = reservable::find($request->reservable_id);
		if(Auth::user()->accountType == "admin"){

			if(isset($_POST['btnSave'])){
				$reservable->type = $request->type;
				$reservable->venueOrEquipment = $request->venueOrEquipment;
				$reservable->details = $request->details;
				$reservable->availableQuantity = $request->availableQuantity;
				$reservable->exemptedvpaa = ($request->exemptedvpaa == true) ? 1: 0;
				
				$reservable->serial = (count($request->serial) > 0) ? $request->serial:null;
				$reservable->save();
			}
			
			return redirect("/edit/" . $reservable->id)->with('affirm', $reservable->venueOrEquipment . ' updated successfully');
		}
		//else
		return view("/");
	}
	public function getEdit($id){
		if(Auth::user()->accountType == "admin"){

			$data['reservable'] = reservable::find($id);
			return view("edit",$data);
		}
		//else
		return redirect("/");
	}
	public function postVenuesEquipment(Request $request){
		$reservable = reservable::find($request->reservable_id);
		if(isset($_POST['btnEdit'])){
			return redirect("/edit/" . $reservable->id);
		}
		if(isset($_POST['btnDisable'])){
			$reservable->enabled = false;
			$reservable->save();
			return redirect('/venuesEquipment')->with('affirm', $reservable->venueOrEquipment . ' successfully disabled');
		}
		if(isset($_POST['btnEnable'])){
			$reservable->enabled = true;
			$reservable->save();
			return redirect('/venuesEquipment')->with('affirm', $reservable->venueOrEquipment . ' successfully enabled');
		
		}

		if(isset($_POST['btnDelete'])){
			$name = $reservable->venueOrEquipment;
			$reservable->delete();	
			$data['reservableVenues'] = reservable::where('type', 'venue')->get();
			$data['reservableEquipments'] = reservable::where('type', 'equipment')->get();
			$data['reservableSupportService'] = reservable::where('type','Support Services')->get();
			return redirect('/venuesEquipment')->with('affirm', $name . ' successfully enabled');

		}

		$data['reservables'] = reservable::all();
		
		return view("venuesEquipment", $data);
	}
	public function getVenuesEquipment(){
		$data['reservableVenues'] = reservable::where('type', 'venue')->get();
		$data['reservableEquipments'] = reservable::where('type', 'equipment')->get();
		$data['reservableSupportService'] = reservable::where('type','Support Services')->get();
		return view("venuesEquipment", $data);
	}
	public function postReservations(Request $request){
		$reservation = reservation::find($request->reservation_id);
		$approvallog = "";
		if(isset($_POST['btnClose'])){
			$reservation->setStatus("closed");
		}

		if(isset($_POST['btnApprove'])){
			$reservation->setApproved(Auth::user()->accountType);

			$approvallog = new approvallog();
			$approvallog->reservation_id = $reservation->id;
			$approvallog->setStatusApproved($reservation->status, $reservation->process_id);

		}

		if(isset($_POST['btnDecline'])){
			
			$reservation->setStatus("disapproved");

			$approvallog = new approvallog();
			$approvallog->reservation_id = $reservation->id;
			$admin = Auth::user()->accountType;
			$approvallog->setStatusDisapproved("disapproved by $admin");

		}
		$allReservations = reservation::all();
		if(isset($_POST['btnSaveRemarks'])){
			$reservation->remarks = $request->saveRemarks;
		}
		$reservation->save();
		$approvallog->save();

		$paramStatus = "";
		switch(Auth::user()->accountType){
			
			case "Program Head":
			$paramStatus = "pending Program Head";
			break;
			case "Dean":
			$paramStatus = "pending Dean";
			break;
			
			case "VPAA":
			$paramStatus = "pending VPAA";
			break;
			case "admin":
			$paramStatus = "pending admin";
			break;
		}
		$data['reservations'] = reservation::where('status',$paramStatus)->orderBy('id','desc')->get();

		return view('reservations',$data);
	}
	
	public function getAllReservation(Request $request){
		$paramStatus = "approved";
		
		if(isset($_GET['dateStart'])){
			$data['reservations'] = reservation::where('status',$paramStatus)->where('dateStart',$request->dateStart)->get();
		
		} else{
			$data['reservations'] = reservation::where('status',$paramStatus)->orderBy('id','desc')->get();
		
		}
		return view('allreservations',$data);
	}
	public function getReservations(Request $request){
		$paramStatus = "";
		switch(Auth::user()->accountType){
			
			case "Program Head":
			$paramStatus = "pending Program Head";
			break;
			case "Dean":
			$paramStatus = "pending Dean";
			break;
			case "ITD":
			$paramStatus = "pending ITD";
			break;
			case "VPAA":
			$paramStatus = "pending VPAA";
			break;
			case "admin":
			$paramStatus = "pending admin";
			break;
		}
		if(isset($_GET['dateStart'])){
			$data['reservations'] = reservation::where('status',$paramStatus)->where('dateStart',$request->dateStart)->get();
		
		} else{
			$data['reservations'] = reservation::where('status',$paramStatus)->orderBy('id','desc')->get();
		
		}
		return view('reservations',$data);
	}
	public function getMyReservation(){
		$data['reservations'] = reservation::where('user_id', Auth::user()->id)->orderBy('updated_at','desc')->get();
		
		return view('myreservation',$data);
	}
	public function getIndex(){
        if ( ! Auth::user()->active) {
            Auth::logout();
            // return with error
            return \Redirect::route('login')->withErrors(
            array('Account not yet activated by admin')
            );  
        }

		$data['reserveableRooms'] = reservable::where('enabled', true)->where('type', '!=', 'equipment')->get();
		
		if(Auth::check()){
			if(Auth::user()->accountType == "normal" ||  Auth::user()->accountType == "faculty"){
				return view('home', $data);
			}
			else{
				return redirect("/reservations");
		
			}
		} else{
			return redirect('/auth/login');
		}
		
	}
	public function getReservable($id){
		if(Auth::check()){
			if(Auth::user()->accountType != "normal"){
				return redirect("/reservations");
			}
		}
		$data['reservable'] = reservable::find($id);
		if(reservable::find($id)){
			return view('reservable', $data);

		}

	}
	public function postReservable(Request $request){
		$data['reservable'] = reservable::find($request->reservable_id);
		if($data['reservable']){
			//validation
			//$request->session()->flash('error', 'Cannot reserve on the specified date and time because it conflicts with existing reservation schedule	');
			//return view('reservable', $data);
			$dateStart = $request->input('dateStart');
		$timeStart = $request->input('timeStart') . ":00";
		$dateEnd = $request->input('dateEnd');
		$timeEnd = $request->input('timeEnd') . ":00";

		$dateTimeStart = $dateStart . " " . $timeStart;
		$dateTimeEnd = $dateEnd . " " . $timeEnd;
			$reservation = reservation::create(
				[
					'reservable_id' => $request->reservable_id,
					'attendants' => $request->attendants,
					'user_id' => Auth::user()->id,
					'dateTimeStart' => $dateTimeStart,
					'dateTimeEnd' => $dateTimeEnd,
					'status' => 'pending ITD',
					'reason' => $request->reason
				]
			);

			return redirect("/")->with("affirm", "Request for reservation is now processed. Please wait for approval.");
		}

	}
	public function getCheckAvailability(Request $request){
		$dateStart = $request->input('dateStart');
		$timeStart = $request->input('timeStart') . ":00";
		$dateEnd = $request->input('dateEnd');
		$timeEnd = $request->input('timeEnd') . ":00";
		$reservable_id = $request->input('reservable_id');
		$dateTimeStart = $dateStart . " " . $timeStart;
		$dateTimeEnd = $dateEnd . " " . $timeEnd;
$date = date_create($timeEnd);
date_sub($date, date_interval_create_from_date_string('1 minute'));
$dateTimeEnd = date_format($date, 'Y-m-d H:i:s');
		$conflictingReservation = reservation::where('reservable_id',$reservable_id)->where('dateTimeStart', '<=', $dateTimeEnd)->where('dateTimeStart', '>=', $dateTimeStart)->where('status','!=', 'closed')->get();
		$conflictingReservation2 = reservation::where('reservable_id',$reservable_id)->where('dateTimeEnd', '<=', $dateTimeEnd)->where("dateTimeEnd", ">=", $dateTimeStart)->where('status','!=', 'closed')->get();
		//if no conflict
		if(is_null($conflictingReservation->first()) && is_null($conflictingReservation2->first()) ){
			
			$data = [
				'message' => "<p style = 'color:green'>Available.</p>",
				'output' => true
			];
			return response()->json( $data );
		}
		if(!is_null($conflictingReservation->first()) ){
			$cid = $conflictingReservation->first()->id;
			$ConflictdateTimeStart = $conflictingReservation->first()->dateTimeStart;
			$ConflictdateTimeEnd = $conflictingReservation->first()->dateTimeEnd;
			$data = [
				'message' => "<p style = 'color:red'>not available due to reservation number: $cid; from $ConflictdateTimeStart to $ConflictdateTimeEnd</p>",
				'output' => false
			];
			return response()->json( $data );
	
		}
		if(!is_null($conflictingReservation2->first()) ){
			$cid = $conflictingReservation2->first()->id;
			$ConflictdateTimeStart = $conflictingReservation2->first()->dateTimeStart;
			$ConflictdateTimeEnd = $conflictingReservation2->first()->dateTimeEnd;
			$data = [
				'message' => "<p style = 'color:red'>not available due to reservation number: $cid; from $ConflictdateTimeStart to $ConflictdateTimeEnd</p>",
				'output' => false

			];
			return response()->json( $data );
	
		}
		
	}
}
?>
<?php
Route::post('/activate/{id}', [
	'uses' => 'HomeController@postActivate',
		'middleware' => 'auth'
		]
	);

Route::get('/courses', 'HomeController@getcourses');
Route::get('/course/{id}', 'HomeController@getcourse');
Route::post('/course/{id}', 'HomeController@postcourse');
Route::get('/newcourse', 'HomeController@getNewcourse');
Route::post('/newcourse', 'HomeController@postNewcourse');

Route::get('/getReservableRooms', 'HomeController@getReservableRooms');
Route::get('/getAvailableQuantity', 'HomeController@getAvailableQuantity');
Route::get('/getAvailableEquipment', 'HomeController@getAvailableEquipment');
Route::get('/getAvailableSupportService', 'HomeController@getAvailableSupportService');
Route::post('/apply', 'HomeController@postApply');
Route::get('/getUserByUsernamePassword', 'HomeController@getUserByUsernamePassword');
Route::get('/courses', 'HomeController@getCourses');
Route::get('/departments', 'HomeController@getDepartments');
Route::get('/department/{id}', 'HomeController@getDepartment');
Route::post('/department/{id}', 'HomeController@postDepartment');
Route::get('/newdepartment', 'HomeController@getNewDepartment');
Route::post('/newdepartment', 'HomeController@postNewDepartment');
Route::post('/registerUser', 'HomeController@postRegisterUser');
Route::resource('/reservation', 'ReservationController');
Route::post('/cleararchivedreservation', [
	'uses' => 'HomeController@postClearArchivedReservation',
		'middleware' => 'auth'
		]
	);
Route::post(
	'/deleteAccount/{id}',[
		'uses' => 'HomeController@postDeleteAccount',
		'middleware' => 'auth'
	]
	);

Route::post(
	'/addAccount',[
		'uses' => 'HomeController@postAddAccount',
		'middleware' => 'auth'
	]
	);

Route::get(
	'/addAccount',[
		'uses' => 'HomeController@getAddAccount',
		'middleware' => 'auth'
	]
	);

Route::post(
	'/editAccount/{id}',[
		'uses' => 'HomeController@postEditAccount',
		'middleware' => 'auth'
	]
	);

Route::get(
	'/editAccount/{id}',[
		'uses' => 'HomeController@getEditAccount',
		'middleware' => 'auth'
	]
	);
Route::get(
	'/userMaintenance',[
		'uses' => 'HomeController@getUserMaintenance',
		'middleware' => 'auth'
	]
	);

Route::get(
	'/allreservation',[
		'uses' => 'HomeController@getAllReservation'
	]
	);
Route::get(
	'/toggleNotification',[
		'uses' => 'HomeController@getToggleNotification',
		'middleware' => 'auth'
	]
	);
Route::get(
	'/archivedreservations',[
		'uses' => 'HomeController@getArchivedReservation',
		'middleware' => 'auth'
	]
	);
Route::get(
	'/getRooms',[
		'uses' => 'HomeController@getRooms',
		'middleware' => 'auth'
	]
	);
Route::get(
	'/new',[
		'uses' => 'HomeController@getNew',
		'middleware' => 'auth'
	]
	);
Route::post(
	'/new',[
		'uses' => 'HomeController@postNew',
		'middleware' => 'auth'
	]
	);
Route::post(
	'/myaccount',[
		'uses' => 'HomeController@postMyAccount',
		'middleware' => 'auth'
	]
	);
Route::get(
	'/myaccount',[
		'uses' => 'HomeController@getMyAccount',
		'middleware' => 'auth'
	]
	);
Route::post(
	'/edit/{id}',[
		'uses' => 'HomeController@postEdit',
		'middleware' => 'auth'
	]
	);
Route::get(
	'/edit/{id}',[
		'uses' => 'HomeController@getEdit',
		'middleware' => 'auth'
	]
	);
Route::get(
	'/venuesEquipment',[
		'uses' => 'HomeController@getVenuesEquipment',
		'middleware' => 'auth'
	]
	);
Route::get(
	'/systemMaintenance',[
		'uses' => 'HomeController@getSystemMaintenance',
		'middleware' => 'auth'
	]
	);
Route::post(
	'/systemMaintenance',[
		'uses' => 'HomeController@postSystemMaintenance',
		'middleware' => 'auth'
	]
	);
Route::post(
	'/venuesEquipment',[
		'uses' => 'HomeController@postVenuesEquipment',
		'middleware' => 'auth'
	]
	);
Route::post(
	'/reservations',[
		'uses' => 'HomeController@postReservations',
		'middleware' => 'auth'
	]
	);
Route::get(
	'/reservations',[
		'uses' => 'HomeController@getReservations',
		'middleware' => 'auth'
	]
	);
Route::get(
	'/myreservation',[
		'uses' => 'HomeController@getMyReservation',
		'middleware' => 'auth'
	]
	);
Route::get('/', array(
	'as' => 'Home',
	'uses' => 'HomeController@getIndex',
	'middleware' => 'auth'
));
Route::get('/home', array(
	'as' => 'Home',
	'uses' => 'HomeController@getIndex',
	'middleware' => 'auth'
));
Route::post('/', array(
	'as' => 'Home',
	'uses' => 'HomeController@postIndex',
	'middleware' => 'auth'
));
Route::post('/home', array(
	'as' => 'Home',
	'uses' => 'HomeController@postIndex',
	'middleware' => 'auth'
));
Route::post('login', array(
	'uses' => 'AuthController@postIndex'
)
);

Route::get('/reserve/{id}', array(
	'as' => 'Home',
	'uses' => 'HomeController@getReservable',
	'middleware' => 'auth'
));
Route::post('/reserve/{id}', array(
	'as' => 'Home',
	'uses' => 'HomeController@PostReservable',
	'middleware' => 'auth'
));
Route::get('/checkAvailability', array(
	'uses' => 'HomeController@getCheckAvailability'
	));
// Authentication routes...
Route::get('auth/login', ['uses' =>'Auth\AuthController@getLogin', 
	'as' => 'login']);
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

?>
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reserved extends Model
{
    //
    protected $table = "reserved";
    protected $fillable = ['reservation_id','reservable_id','quantity', 'dateTimeStart', 'dateTimeEnd','status'];
    public function reservation(){
    	return reservation::find($this->reservation_id);
    }
    public function amenity(){
    	return reservable::find($this->reservable_id);
    }
    public function venueOrEquipment(){
    	$output= reservable::find($this->reservable_id); 
    	return $output['venueOrEquipment'];
    }
}

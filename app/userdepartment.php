<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userdepartment extends Model
{
    //
    protected $table = 'userdepartment';
    protected $fillable = ['department_id', 'course_id'];
   public function departmentName(){
   	$department = departments::find($this->department_id);
   	$department_name = $department->department;
   	return $department_name;
   }
}

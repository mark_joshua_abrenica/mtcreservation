<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class courses extends Model
{
 protected $table = 'courses';
    protected $fillable = ['id', 'course', 'course_desc','department_id'];

}
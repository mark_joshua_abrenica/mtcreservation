<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reservation extends Model
{
    //
    protected $table = "reservation";
    protected $fillable = ['attendants','user_id','dateTimeStart', 'dateTimeEnd', 'status', 'reason','venue','purpose', 'event', 'notify', 'dateStart', 'remarks','contactnumber', 'process_id'];
    
    public function getApprovallog(){
        $output = approvallog::where('reservation_id', $this->id)->get();
        return $output;
    }
    public function user(){
    	return User::find($this->user_id);
    }
    public function equipments(){
    	$getInEquipment = reserved::where('reservation_id', $this->id)->get();
    	$inEquipment = [];
    	foreach($getInEquipment as $ItemInEquipment){
    		array_push($inEquipment, $ItemInEquipment->reservable_id);
    	}
    	return reservable::whereIn('id', $inEquipment)->get();
    }
    public function reservedEquipments(){
        return reserved::where('reservation_id', $this->id)->get();
    }
    public function venue(){
       
    	$output =  reservable::find($this->venue);
        return $output;
    }
    public function setStatus($status){
        $this->status = $status;
        $this->notify = true;
        $equipments = $this->reservedEquipments();
        foreach($equipments as $equipment){
            $equipment->status = $status;

            $equipment->save();
        }
    }
    public function getDepartment(){
        $student = User::find($this->user_id);
        $course = courses::find($student->course_id);
        $department_id = $course->department_id;

        return $department_id;

    }
    public function getCourse(){
        $student = User::find($this->user_id);
        $output = $student->course_id;
        
        return $output;

    }
    public function hasEquipment(){
        $reserved_equipments = reserved::where('reservation_id', $this->id)->get();
        foreach($reserved_equipments as $r){
            $reservable = reservable::find($r->reservable_id);
            if($reservable->type = "equipment"){
                return true;
            }
        }
        return false;
    }
    public function setApproved($by){
        $venue = reservable::find($this->venue);
        if($this->process_id == 1){
            switch($by){
                case "Program Head":
                $this->setStatus("pending Dean");

                break;
                // case "Dean":
                // $this->setStatus("pending ITD");

                // break;

                case "Dean":
                if($venue->exemptedvpaa == 1){
                    $this->setStatus("pending admin");
                    if($this->hasEquipment()){
                        $this->setStatus("pending VPAA");
                        
                    }
                }else{
                    $this->setStatus("pending VPAA");
                }
                break;
                case "VPAA":
                $this->setStatus("pending admin");

                break;
                case "admin":
                $this->setStatus("approved");
                break;
            }
        } else if ($this->process_id == 2){
            switch($by){
                case "Program Head":
                $this->setStatus("pending Dean");

                break;
                // case "Dean":
                // $this->setStatus("pending ITD");

                // break;

                case "Dean":
                $this->setStatus("pending admin");
                
                break;
                // case "VPAA":
                // $reservation->setStatus("pending admin");

                // break;
                case "admin":
                $this->setStatus("approved");
                break;
            }
        }
    }
}

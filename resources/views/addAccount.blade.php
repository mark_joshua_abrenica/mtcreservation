@extends('layout.main4')
@section('content')
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div style = "color:green;">{{ $error }}</div>
  @endforeach
@endif

			<div class="content">
			    <h3>Add new User account</h3>
			    <br><a href = "/userMaintenance">Back to user maintenance</a><br><br>
				<form class="pure-form pure-form-aligned" method="POST" action="">
					 {!! csrf_field() !!}
				    <fieldset>

				        <div class="pure-control-group">
				            <label for="name">Account type</label>
				            <select name = "accountType">
				            	<option value = "normal">Student</option>
				            	<option value = "faculty">Faculty</option>
				            	<option value = "Program Head">Program Head</option>
				            	<option value = "Dean">Dean</option>
				            	<option value = "VPAA" >VPAA</option>
				            	<option value = "admin">Admin</option>
				            </select>
				        </div>
			        
				        <div class="pure-control-group">
				            <label for="name">Username</label>
				            <input id="name" required name = "name" type="text" placeholder="Username" value = "{{ (isset($_POST['name'])) ? $_POST['name'] : '' }}"/>
				        </div>
			        	<div class="pure-control-group">
				            <label for="name">Email</label>
				            <input id="email" required name = "email" type="email" placeholder="Email"  value = "{{ (isset($_POST['email'])) ? $_POST['email'] : '' }}"/>
				        </div>

				         <div class="pure-control-group">
				            <label for="password">Password</label>
				            <input id="password" required name = "password" type="password" placeholder="Password"/>
				        </div>
				        <div class="pure-control-group">
				            <label for="password">Retype password</label>
				            <input id="password" required name = "password_confirmation" type="password" placeholder="Password"/>
				        </div>


				         <div class="pure-control-group">
				            <label for="firstname">First name</label>
				            <input id="firstname" required name = "firstname" type="text" placeholder="first name"  value = "{{ (isset($_POST['firstname'])) ? $_POST['firstname'] : '' }}"/>
				        </div>
				         <div class="pure-control-group">
				            <label for="lastname">Last name</label>
				            <input id="lastname" required name = "lastname" type="text" placeholder="first name"  value ="{{ (isset($_POST['lastname'])) ? $_POST['lastname'] : '' }}"/>
				        </div>
				        <div class="pure-control-group">
				            <label for="middlename">Middle name</label>
				            <input id="middlename"  name = "middlename" type="text" placeholder="Middle  name"  value = "{{ (isset($_POST['middlename'])) ? $_POST['middlename'] : '' }}"/>
				        </div>
				        
				        <div class="pure-control-group">
				            <label for="middlename">Course</label>
				            <select name = "course">
				            <option value = "">No Course</option>
				            
				            @foreach($courses as $course)
				            <option value = "{{$course->id}}"  {{ (isset($_POST['course']) && $_POST['course'] == $course->id) ? 'selected' : '' }}>{{$course->course}}</option>
				            @endforeach
				            </select>
				        </div>

				         <div class="pure-control-group">
				            <label for="middlename">Department</label>
				            <select name = "department">
				            <option value = "">No department</option>
				            
				            @foreach($departments as $department)
				            <option value = "{{$department->id}}"   {{ (isset($_POST['department']) && $_POST['department'] == $department->id) ? 'selected' : '' }}>{{$department->department}}</option>
				            @endforeach
				            </select>
				        </div>
				        <div class="pure-control-group">
				        	
				        </div>
				         
				        <div class="pure-controls">
				            <button type="submit" name = 'btnSave' class="pure-button pure-button-primary">Register new account</button>
				        </div>
				    </fieldset>
				</form>
				<!-- @if(Auth::user()->accountType != "normal")
				<h3 style = "margin-top:10px;margin-bottom:10px;">Assigned Department(s) [not yet finished]</h3>
					<ul>
						@foreach(Auth::user()->getDepartments() as $department)
						<li>{{$department->department_desc}}</li>
						@endforeach
						
					</ul>
				 {!! App\viewStrategy::departments(Auth::user()->id) !!}
				 @endif
 -->
			</div><!-- class='content'-->
	
@stop
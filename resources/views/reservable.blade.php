@extends('layout.main4')

@section('content')
	@if(session('error'))
	<p style = "color:red;">{{session('error')}}</p>
	@endif
<div class="col-sm-6">
<img class="img-responsive" src = "{{ asset($reservable->picture) }}" />
</div>
<form id = "form" action = '' method = 'post' onsubmit="return check()"><br>
{!! csrf_field() !!}
<input id = 'reservable_id' type = 'hidden' name = 'reservable_id' value = '{{$reservable->id}}'/>

<table class="pure-table pure-table-striped">
<tr>
  <td align = "right" style = "width:100px;padding-right:10px;"><b>name:</b></td><td>{{$reservable->venueOrEquipment}}</td>
</tr>
<tr>
  <td align = "right" style = "width:100px;padding-right:10px;"><b>type:</b></td><td>{{$reservable->type}}</td>
</tr>
<tr>
  <td align = "right" style = "width:100px;padding-right:10px;"><b>details:</b></td><td>{{$reservable->details}}</td>
</tr>
<tr>
  <td align = "right" style = "width:100px;padding-right:10px;"><b>Please Enter reason/event:</b></td><td><input required type = 'text' name = 'reason' placeholder = 'reason/event'  /></td>
</tr>
 <tr>
  <td align = "right" style = "width:100px;padding-right:10px;"><b>date/time of reservation:</b></td><td><input id = 'dateStart' type ='date' required name = 'dateStart' value = "{{ (isset($_POST['dateStart'])) ? $_POST['dateStart'] : ''}}"/> <input id = 'timeStart' type = 'time' name = 'timeStart' required value = "{{ (isset($_POST['timeStart'])) ? $_POST['timeStart'] : ''}}"/></td>
</tr>
 <tr>
  <td align = "right" style = "width:100px;padding-right:10px;"><b>date/time of end reservation:</b></td><td><input id = 'dateEnd' type ='date' required name = 'dateEnd' value = "{{ (isset($_POST['dateEnd'])) ? $_POST['dateEnd'] : ''}}"/> <input id = 'timeEnd' type = 'time' name = 'timeEnd' required value = "{{ (isset($_POST['timeEnd'])) ? $_POST['timeEnd'] : ''}}"/></td>
</tr>
<tr>
  <td colspan = '2'>
    <button type = 'button' onclick = "check()" name = 'btnCheck' id = "btncheck">Check availability</button>
    <div id = "result">

    </div>
    <div id = "proceed">
      <textarea name = 'attendants' visibility = "hidden" placeholder="attendants"></textarea>
      <input type = 'submit'  name = 'btnReserve' value = 'reserve'/>
    </div>

  </td>
</tr>

</form>
</table>
<script>
$('#proceed').hide();
function check(){
    
        var token = document.getElementsByName('_token').value;
        $.ajax({
          type: 'GET',
          headers: {'X-CSRF-TOKEN': token},
          url: '../checkAvailability',
          data: { 'dateStart' : $("#dateStart").val(), 'dateEnd' : $("#dateEnd").val(), 'timeStart' : $("#timeStart").val(),'timeEnd' : $("#timeEnd").val(), 'reservable_id' : $("#reservable_id").val() },
          dataType: 'json',
          success: function(message) {
              
              $("#result").html(message.message);
              if(message.output){
                $('#proceed').show();
              } else{
              $('#proceed').hide();
                
              }
              return message.output;
          },
          error: function(j) {
            alert('Error loading. Please complete the form');
            return false;
          }
        });
    
}
$('#form').submit(function(){
  if(!$(this).attr('validated'))
    {
      var token = document.getElementsByName('_token').value;
        $.ajax({
          type: 'GET',
          headers: {'X-CSRF-TOKEN': token},
          url: '../checkAvailability',
          data: { 'dateStart' : $("#dateStart").val(), 'dateEnd' : $("#dateEnd").val(), 'timeStart' : $("#timeStart").val(),'timeEnd' : $("#timeEnd").val(), 'reservable_id' : $("#reservable_id").val() },
          dataType: 'json',
          success: function(message) {
              if(message.output){
                   $('#form').attr('validated',true);
                   $('#form').submit();
                
              }
              else{
                alert("Can't reserve on the selected date/time schedule range");
              }
              
          },
          error: function(j) {
           alert('Error loading. Please complete the form');
            return false;
          }
        });
      return false;
    }
    return true;
});
                  </script> 
@stop
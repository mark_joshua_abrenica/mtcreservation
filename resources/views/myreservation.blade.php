@extends('layout.main4')

@section('content')
<h1 style = "margin-bottom:20px;">My Reservation</h1>
 <table class = "pure-table">
	<thead>
		<tr>
			<th>Date requested</th>
			<th>purpose</th>
			<th>event</th>
			<th>attendants</th>
			<th>venue</th>
			<th>Equipment / Support Services</th>
			<th>date time start</th>
			<th>date time end</th>
			<th>status</th>
			<th>remarks</th>
			<th>notify</th>
		</tr>
	</thead>
	<?php $i = 0; ?>
	@foreach($reservations as $reservation)
	<?php $i++; ?>
		<tr {!! ($i % 2 == 0) ?  "class='pure-table-odd'" : "" !!}> 
			<td>{{$reservation->created_at->format("Y-m-d")}}</td>
			<td>{{$reservation->purpose}}</td>
			<td>{{$reservation->event}}</td>
			<td>{{$reservation->attendants}}</td>
			<td>{{( $reservation->venue() ) ? $reservation->venue()->venueOrEquipment : ""}}</td>
			<td>
				<ul>
				@foreach($reservation->reservedEquipments() as $equipment)
				<li>{{$equipment->venueOrEquipment()}} ({{$equipment->quantity}})</li>
				@endforeach
				</ul>
			</td>
			<td>{{date_format(new DateTime($reservation->dateTimeStart), "Y-m-d / h:i:s A")}}</td>
			<td>{{date_format(new DateTime($reservation->dateTimeEnd), "Y-m-d / h:i:s A")}}</td>
			<td>
				@if($reservation->status != "pending Program Head")
				<ul  style ="width:300px; font-size:12px;">
				@foreach($reservation->getApprovallog() as $status)
				<li>{{$status->status}} ({{$status->created_at->format('m-d-Y h:i a')}})</li>
				@endforeach
				</ul>
			@else
			{{$reservation->status}}
			@endif
			</td>
			<td>{{$reservation->remarks}}</td>
			<td>
				<form action = '' method = 'post'>
				{!! csrf_field() !!}
				<input type = 'hidden' name = 'reservation_id{{$reservation->id}}' value = '{{$reservation->id}}' id = 'reservation_id{{$reservation->id}}' />
				<input id = 'notifyReservation{{$reservation->id}}' type = 'checkbox' name = 'notifyReservation{{$reservation->id}}' {{($reservation->notify) ? "checked" : ""}} />
				</form>
			</td>
		</tr>

		<script type="text/javascript">
    $(document).ready(function(){
    	var token = document.getElementsByName('_token').value;
        $("#notifyReservation{{$reservation->id}}").change(function() { 
        	var isChecked = $("#notifyReservation{{$reservation->id}}").is(":checked") ? 1:0; 
                $.ajax({
                    url: '../toggleNotification',
                    type: 'GET',
                     headers: {'X-CSRF-TOKEN': token},
                    data: { "reservation_id": $("#reservation_id{{$reservation->id}}").val(),'notify': isChecked, "user_id":{{Auth::user()->id}} },
                    dataType: 'json',
		          success: function(j) {

		           //    $("#notification").html(j);
		          	// if(j==0){
		          	// 	$("#notification").hide();

		          	// 	$("#notification2").hide();

		          	// } else{

		          	// 	$("#notification").show();
		          	// 	$("#notification").css("background-color", "red");
		          	// 	$("#notification").css("text-align", "center");
		          	// 	$("#notification").css("margin-top","-10px");
		          	// 	$("#notification").css("margin-top","-10px");
		          	// 	$("#notification").css("width","32px");
		          	// 	$("#notification").css("color","white");
		          	// 	$("#notification").css("float","right");

		          	// 	$("#notification2").show();
		          	// 	$("#notification2").css("background-color", "red");
		          	// 	$("#notification2").css("text-align", "center");
		          	// 	$("#notification2").css("margin-top","-10px");
		          	// 	$("#notification2").css("margin-top","-10px");
		          	// 	$("#notification2").css("width","32px");
		          	// 	$("#notification2").css("color","white");
		          	// 	$("#notification2").css("float","right");

		          	// }

		          },
		          error: function(j) {
		            alert('Error loading');
		          }
                });

        }); 
    });
</script>
	@endforeach
 </table>

@stop
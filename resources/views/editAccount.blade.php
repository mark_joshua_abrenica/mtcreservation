@extends('layout.main4')
@section('content')
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div style = "color:green;">{{ $error }}</div>
  @endforeach
@endif

			<div class="content">
			    <h3>Edit Account ({{$user->lastname}}, {{$user->firstname}})</h3>

			    <br><a href = "/userMaintenance">Back to user maintenance</a> &nbsp&nbsp&nbsp<a href = "/addAccount">Register new account</a><br><br>
				<form class="pure-form pure-form-aligned" method="POST" action="">
					 {!! csrf_field() !!}
				    <fieldset>

				        <div class="pure-control-group">
				            <label for="name">Account type</label>
				            <select name = "accountType">
				            	<option value = "normal" {{ ($user->accountType=="normal") ? "selected" : ""}}>Student</option>
				            	<option value = "Program Head" {{ ($user->accountType=="Program Head") ? "selected" : ""}}>Program Head</option>
				            	<option value = "Dean" {{ ($user->accountType=="Dean") ? "selected" : ""}}>Dean</option>
				            	<option value = "ITD" {{ ($user->accountType=="ITD") ? "selected" : ""}}>ITD</option>
				            	<option value = "VPAA" {{ ($user->accountType=="VPAA") ? "selected" : ""}}>VPAA</option>
				            	<option value = "admin" {{ ($user->accountType=="admin") ? "selected" : ""}}>Admin</option>
				            </select>
				        </div>
			        
				        <div class="pure-control-group">
				            <label for="name">Username</label>
				            <input id="name" required name = "name" type="text" placeholder="Username" value = "{{ $user->name }}" />
				        </div>
			        	<div class="pure-control-group">
				            <label for="name">Email</label>
				            <input id="email" required name = "email" type="text" placeholder="Email" value = "{{$user->email }}" />
				        </div>

				       

				         <div class="pure-control-group">
				            <label for="firstname">First name</label>
				            <input id="firstname" required name = "firstname" type="text" placeholder="first name" value = "{{ $user->firstname }}" />
				        </div>
				         <div class="pure-control-group">
				            <label for="lastname">Last name</label>
				            <input id="lastname" required name = "lastname" type="text" placeholder="first name" value = "{{$user->lastname}}" />
				        </div>
				        <div class="pure-control-group">
				            <label for="middlename">Middle name</label>
				            <input id="middlename"  name = "middlename" type="text" placeholder="Middle  name" value = "{{$user->middlename}}" />
				        </div>
				        
				        <div class="pure-control-group">
				            <label for="middlename">Course</label>
				            <select name = "course">
				            <option value = ""  {{($user->department_id == null) ? "selected" : ""}} >No Course</option>
				            
				            @foreach($courses as $course)
				            <option value = "{{$course->id}}" {{($user->course_id == $course->id) ? "selected" : ""}}>{{$course->course}}</option>
				            @endforeach
				            </select>
				        </div>

				         <div class="pure-control-group">
				            <label for="middlename">Department</label>
				            <select name = "department">
				            <option value = ""  {{($user->department_id == null) ? "selected" : ""}}>No department</option>
				            
				            @foreach($departments as $department)
				            <option value = "{{$department->id}}" {{($user->department_id == $department->id) ? "selected" : ""}}>{{$department->department}}</option>
				            @endforeach
				            </select>
				        </div>
				        <div class="pure-control-group">
				        	
				        </div>
				         
				        <div class="pure-controls">
				            <button type="submit" name = 'btnSave' class="pure-button pure-button-primary">Save</button>
				        </div>
				    </fieldset>
				</form>
				<!-- @if(Auth::user()->accountType != "normal")
				<h3 style = "margin-top:10px;margin-bottom:10px;">Assigned Department(s) [not yet finished]</h3>
					<ul>
						@foreach(Auth::user()->getDepartments() as $department)
						<li>{{$department->department_desc}}</li>
						@endforeach
						
					</ul>
				 {!! App\viewStrategy::departments(Auth::user()->id) !!}
				 @endif
 -->
			</div><!-- class='content'-->
	
@stop
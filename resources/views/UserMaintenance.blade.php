@extends('layout.main4')

@section('content')
<h1 style = "margin-bottom:20px;">User Accounts</h1>
<a href = "/addAccount">Register new account</a><br><br>
 <table class = "pure-table">
	<thead>
		<tr>
			<th>Account type:</th>
			<th>Department</th>
			<th>Course</th>
			<th>User name</th>
			<th>Last name</th>
			<th>First name</th>
			
			
			<th>action</th>
		</tr>
	</thead>
	
	<?php 
	$i = 0; 
	 ?>
	@foreach($users as $user)
	<?php
	$i++;
	?>
		<tr {{($user->active == false) ? 'style = color:red; ' : ''}} {!! ($i % 2 == 0) ?  "class='pure-table-odd'" : "" !!}> 
			<td>{{($user->accountType == "normal") ? "student" :$user->accountType}}</td>
			<td>{{$user->getAdminDepartment()}}</td>
			<td>{{$user->getAdminCourse()}}</td>
			<td>{{$user->name}}</td>
			<td>{{$user->lastname}}</td>
			<td>{{$user->firstname}}</td>
			
			<td>
				<form action = '../editAccount/{{$user->id}}' method = 'GET'>
					{!! csrf_field() !!}
					<input type = 'hidden' name = 'user_id' value = '{{$user->id}}' />
					<input type = 'submit' value = 'edit' name = 'btnEditUser'/>
				</form>
				<form action = '../deleteAccount/{{$user->id}}' method = 'POST'>
					{!! csrf_field() !!}
					<input type = 'hidden' name = 'user_id' value = '{{$user->id}}' />
					<input type = 'submit' value = 'delete' name = 'delete' onclick="return confirm('Are you sure you want to delete this user?')"/>
				</form>
				<form action = '../activate/{{$user->id}}' method = 'POST'>
					{!! csrf_field() !!}
					<input type = 'hidden' name = 'user_id' value = '{{$user->id}}' />
					@if($user->active)
					<input type = 'submit' value = 'deactivate' name = 'btnDeactivate' onclick="return confirm('Are you sure you want to deactivate this user?')"/>
					@else
					<input type = 'submit' value = 'activate' name = 'btnActivate' onclick="return confirm('Are you sure you want to activate this user?')"/>
					@endif
				</form>
			</td>
	@endforeach
 </table>
@stop
@extends('layout.main2')

@section('content')

<!-- banner Slider starts Here -->
<script src="{{asset('js/responsiveslides.min.js')}}"></script>
<script>
    $(function () {
      $("#slider").responsiveSlides({
      	auto: true,
      	speed: 500,
        namespace: "callbacks",
        pager: false,
        nav:true,
      });
    });
   
</script>

<!--gallery start here-->
<div class="gallery" id="gallery">
	<div class="container">
		@if (session('affirm'))
		    <div class="alert alert-success">
		        {{ session('affirm') }}
		    </div>
		@endif
		
		<div class="row" style = "margin-top:-30px">
                        <div class="col-lg-10 col-lg-offset-1 form-box">
                          <h4>TYTANA RESERVATION</h4>
                           <i><img width = '100px' src = "{{asset('images/logo.jpg')}}"></img></i>
                          <form role="form"  id ="registration-form" class="registration-form" enctype="multipart/form-data" name = 'applicationform' class="pure-form pure-form-aligned" method="POST" action="">
                            
                       
                            <fieldset>
                               {!! csrf_field() !!}
                               <input type = 'hidden' name = 'userdepartment' id = 'userdepartment'/>
                              <div class="form-top">
                                <div class="form-top-left">
                                 
                                    
                                </div>
                                <div class="form-top-right">
                                 
                                </div>
                                </div>
                                <style>
                                td{
                                  text-align:left;
                                  height:60px;
                                }
                                .column1{
                                  width:130px;
                                }
                                </style>
                                
                               
                                  <div class="form-group pure-u-1 pure-u-md-1-3">
                                <div class="form-bottom pure-g">

                                  <table class = "form-group pure-u-1 pure-u-md-1-3">
                                  <tr><td colspan = "4" width = '100px'>
                                    <b>{{Auth::user()->lastname}}, {{Auth::user()->firstname}} </br> {{Auth::user()->getCourse()}}</b></td></tr>
                                  <tr width = '100px'><td>Date requested:</td><td colspan = '3'>{{date("n/j/Y")}} </td></tr>
                                  <tr><td class = 'column1'>Event</td><td colspan = '3'><input style = "width:100%" required type = 'text' name = 'event'/></td></tr>
                                  <tr><td class = 'column1'>Date of event:</td><td  style = "width:145px;"><input style = "width:145px;margin-right:10px;" id = 'dateStart' type ='date' required name = 'dateStart' value = "{{ (isset($_POST['dateStart'])) ? $_POST['dateStart'] : ''}}"/></td><td style = "width:90px">Attendants: </td><td><input style = "width:108px;" required type = 'number' name = 'attendants' id = 'attendants'/></td> </tr>
                                  <tr><td class = 'column1'>time start:</td><td><input style = "width:145px;" id = 'timeStart' type = 'time' name = 'timeStart' required value = "{{ (isset($_POST['timeStart'])) ? $_POST['timeStart'] : ''}}"/></td><td>Time end:</td><td><input style = "width:108px;" id = 'timeEnd' type = 'time' name = 'timeEnd' required value = "{{ (isset($_POST['timeEnd'])) ? $_POST['timeEnd'] : ''}}"/></td></tr>
                                  <tr><td class = 'column1'>Purpose:</td><td colspan = '3'><textarea style = "width:100%" required name = 'purpose'></textarea></td></tr>
                                  <tr><td class = 'column1'>Contact number:</td><td colspan = '3'><input style = "width:100%;" required type = 'number' name = 'contactnumber'/></td></tr>
                                </table>     
                                  <button type="button" class="btn btn-next" onclick = "populate()">Next</button>
                                </div>
                          </fieldset>
                          
                          <fieldset>
                              <div class="form-top">
                                <div class="form-top-left">
                                  
                                    
                                </div>
                                 <div class="form-top-right">
                                  
                                </div>
                              </div>
                              <div class="form-bottom">
                                
                                <div class="gallery" id="gallery" style = "border-bottom:1px solid #b0b0b0">
                                <h3>VACANT VENUE</h3>
                                <div id = "outputmessagevenue"></div>
                                  
                                <input ID = "SearchVenue" type = 'text' value = '' placeholder = 'search' style = 'margin-bottom:20px;'/> <button type = 'button' class = 'pure-button pure-button-primary' onclick = 'populateVenuesBySearch()'>Search</button><button style = "margin-left:10px;" type = 'button'  class = 'pure-button button-secondary' onclick = "clearSearch()">Clear search</button>
                                  
                                  <div id = "outputrooms">
                  								</div>
							                  </div>
                                 <h3>EQUIPMENTS NEEDED</h3>
                                 <div class="gallery" id="gallery" style = "border-bottom:1px solid #b0b0b0">
                                    <div id = "outputequipment">

                                    </div>
                                  </div>
                                  <h3>SUPPORT SERVICES</h3>
                                  <div class="gallery" id="gallery">
                                    <div id = "outputservices">

                                    </div>
                                  </div>
                                 <button type="button" class="btn btn-previous">Previous</button>
                                  <button type="submit" class="btn" name = 'apply' id = 'apply'>Apply</button>
                              </div>
                          </fieldset>
                          
                        </form>
                        
                        </div>
                    </div>


	</div>
</div>
<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
			<script type="text/javascript" src="js/nivo-lightbox.min.js"></script>
				<script type="text/javascript">
				$(document).ready(function(){
				    $('#nivo-lightbox-demo a').nivoLightbox({ effect: 'fade' });
				});

				
				function populate(){
					 var token = document.getElementsByName('_token').value;
				 $.ajax({
		          type: 'GET',
		          headers: {'X-CSRF-TOKEN': token},
		          url: '../getRooms',
		          data: { 'datetimestart' : $("#dateStart").val() + " " + $("#timeStart").val() + ":00", 'datetimeend' : $("#dateStart").val() + " " + $("#timeEnd").val() + ":00"},
		          dataType: 'json',
		          success: function(j) {
		              
		              $("#outputrooms").html(j.rooms);

		              $("#outputequipment").html(j.equipment);
                  $("#outputservices").html(j.Services);
		          },
		          error: function(j) {
		            alert('Error loading: ' + j[0]);
		          }
		        });
					 //$('#populaterooms').append(output);
				
				}
        // function checkQuantity(e){
        //    var token = document.getElementsByName('_token').value;
        //  $.ajax({
        //       type: 'GET',
        //       headers: {'X-CSRF-TOKEN': token},
        //       url: '../checkQuantity',
        //       data: { 'datetimestart' : $("#dateStart").val() + " " + $("#timeStart").val() + ":00", 'datetimeend' : $("#dateEnd").val() + " " + $("#timeEnd").val() + ":00", "equipmentQuantity" : e, "equipmentID" : $("#").val()},
        //       dataType: 'json',
        //       success: function(j) {
        //           alert(j.message);
        //       },
        //       error: function(j) {
        //         alert('Error loading: ' + j[0]);
        //       }}

        function populateVenuesBySearch(){
           var token = document.getElementsByName('_token').value;
           $.ajax({
              type: 'GET',
              headers: {'X-CSRF-TOKEN': token},
              url: '../getRooms',
              data: { 'datetimestart' : $("#dateStart").val() + " " + $("#timeStart").val() + ":00", 'datetimeend' : $("#dateStart").val() + " " + $("#timeEnd").val() + ":00", "search" : $("#SearchVenue").val()},
              dataType: 'json',
              success: function(j) {
                  
                  $("#outputrooms").html(j.rooms);

                  $("#outputequipment").html(j.equipment);
              },
              error: function(j) {
                alert('Error loading: ' + j[0]);
              }
            });
        }
        function clearSearch() {
            document.getElementById("SearchVenue").value=""
            populate();
        }
				</script>

<!--gallery end here-->

@stop
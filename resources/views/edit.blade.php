@extends('layout.main4')
@section('content')
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div style = "color:green;">{{ $error }}</div>
  @endforeach
@endif

			<div class="content">
			    <h1>Edit</h1>
				<form class="pure-form pure-form-aligned" method="POST" action="" enctype="multipart/form-data">
					 {!! csrf_field() !!}
				    <fieldset>
				    <input type = 'hidden' name = 'reservable_id' value = "{{$reservable->id}}"/>
				        <div class="pure-control-group">
				            <label for="name">Type</label>
				            <select class="pure-input-1-2" name = 'type' required>
				            	<option value = '' >Please select type</option>
				            	<option value = 'venue' {{($reservable->type == "venue") ? "selected":""}}>venue</option>
				            	<option value = 'equipment' {{($reservable->type == "equipment") ? "selected":""}}>equipment</option>

				            	<option value = 'Support Services'  {{($reservable->type == "Support Services") ? "selected":""}}>Support Services</option>
				            </select>
				        </div>
			        	<div class="pure-control-group">
				            <label >Name</label>
				            <input class="pure-input-1-2" required type="text" name = 'venueOrEquipment' placeholder="Equipment name or venue or room number" value = "{{$reservable->venueOrEquipment}}"/>
				        </div>
				        <div class="pure-control-group">
				            <label >Details</label>
				            <textarea class="pure-input-1-2" required name = 'details'>{{$reservable->details}}</textarea>
				        </div>
				         <div class="pure-control-group">
				            <label >Serial</label>
				            <input class="pure-input-1-2" type="text" placeholder="serial key for equipments" name = 'serial' value = "{{ $reservable->serial }}"/>
				        </div>
				        
			           <div class="pure-control-group">
				            <label >Quantity / Capacity</label>
				             <input class="pure-input-1-2" type="number" placeholder="1" name = 'availableQuantity' value = "{{ (old('availableQuantity')) ? old('availableQuantity') : $reservable->availableQuantity}}"/>
				        </div>
				         <div class="pure-control-group">
				            <label >Exempted VPAA Approval</label>
				             <input  type="checkbox"  name = 'exemptedvpaa' {!! ($reservable->exemptedvpaa) ? "checked" : ""  !!}/>
				        </div>
				        <div class="pure-controls">
				            <button type="submit" name = "btnSave" class="pure-button pure-button-primary">Save</button>
				        </div>
				    </fieldset>
				</form>

			</div><!-- class='content'-->
	
@stop
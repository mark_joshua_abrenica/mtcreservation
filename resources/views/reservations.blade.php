@extends('layout.main4')

@section('content')
<h1 style = "margin-bottom:20px;">Reservations</h1>
<form action = '' method = 'get' style = "margin-bottom: 30px;">
<input type = 'date' value = '{{ (isset($_GET["dateStart"])) ? $_GET["dateStart"] : "" }}' name = 'dateStart' />
<input type = 'submit' value = 'filter date'/>
<a style = "margin-left:30px;" href = '/reservations'>Clear date filter</a>
</form>
 <table  class = "pure-table">
	<thead>
		<tr>
			<th>Requested by</th>
			<th>Date Requested</th>
			<th>Contact number</th>
			<th>purpose</th>
			<th>event</th>
			<th>attendants</th>
			<th>venue</th>
			<th>Equipment / Support Services</th>
			<th>date time start</th>
			<th>date time end</th>
			<th>status</th>
			<th>remarks</th>
			<th>action</th>
		</tr>
	</thead>
	<?php $i = 0; ?>
	@foreach($reservations as $reservation)
	<?php
	if(Auth::user()->accountType == "ITD" || Auth::user()->accountType == "admin" || Auth::user()->accountType == "VPAA" ){
		$i++;
	}
	 else if(Auth::user()->accountType == "Program Head"){
	 		if(Auth::user()->checkIfMyCourse($reservation->getCourse())){
	 			$i++;
	 		} else{
	 			continue;
	 		}
	 	} else{
	 		$i++;
	 	}
	 ?>
		<tr {!! ($i % 2 == 0) ?  "class='pure-table-odd'" : "" !!}> 
			<td>{{$reservation->user()->firstname . " " . $reservation->user()->lastname}}</td>
			<td>{{$reservation->created_at->format("Y-m-d")}}</td>
			<td>{{$reservation->contactnumber}}</td>
			<td>{{$reservation->purpose}}</td>
			<td>{{$reservation->event}}</td>
			<td>{{$reservation->attendants}}</td>
			<td>{{($reservation->venue()) ? $reservation->venue()->venueOrEquipment : ""}}</td>
			<td>
				@if($reservation->reservedEquipments())
				<ul>
				@foreach($reservation->reservedEquipments() as $equipment)
				<li>{{$equipment->venueOrEquipment()}} ({{$equipment->quantity}})</li>
				@endforeach
				</ul>
				@endif
			</td>
			<td>{{date_format(new DateTime($reservation->dateTimeStart), "Y-m-d / h:i:s A")}}</td>
			<td>{{date_format(new DateTime($reservation->dateTimeEnd), "Y-m-d / h:i:s A")}}</td>
			<td>
			@if($reservation->status != "pending Program Head")
				<ul  style ="width:300px; font-size:12px;">
				@foreach($reservation->getApprovallog() as $status)
				<li>{{$status->status}} ({{$status->created_at->format('m-d-Y h:i a')}})</li>
				@endforeach
				</ul>
			@else
			{{$reservation->status}}
			@endif

			</td>
			
			<td>
				<form action = '' method = 'post'>
					{!! csrf_field() !!}

				<input type = 'hidden' name = 'reservation_id' value = '{{$reservation->id}}'/>
					<textarea name='saveRemarks'>{{$reservation->remarks}}</textarea>
					<input type = 'submit' value = 'save remarks' name = 'btnSaveRemarks'/>
				</form>
			</td>
			<td><form action = '' method = 'post'>
				{!! csrf_field() !!}
				<input type = 'hidden' name = 'reservation_id' value = '{{$reservation->id}}'/>
				<input type = 'submit' name = 'btnApprove' value = 'approve'/>
				<input type = 'submit' name = 'btnDecline' value = 'disapprove'/>
				
			</form></td>
		</tr>
	@endforeach
 </table>
@stop
@extends('layout.main4')

@section('content')
<h1 style = "margin-bottom:20px;">Manage venues/equipment</h1>
@if(Auth::user()->accountType == "admin")
<a href = "/new" >Add new item</a>
@endif
<h3 style = "margin-top:30px;">Venues</h3>


 <table class = "pure-table">
	<thead>
		<tr>
			<th>Serial</th>
			<th>Type</th>
			<th>Name</th>
			<th>Details</th>
			<th>Capacity</th>
			<th>Enabled</th>
			<th>Exempted of VPAA's approval</th>
			<th>Action</th>
			
		</tr>
	</thead>
	@foreach($reservableVenues as $reservable)
		<tr>
			<td>{{$reservable->serial}}</td>
			<td>{{$reservable->type}}</td>
			<td>{{$reservable->venueOrEquipment}}</td>
			<td>{{$reservable->details}}</td>
			<td>{{$reservable->availableQuantity}}</td>
			<td>{{($reservable->enabled) ? 'enabled':'disabled'}}</td>
			<td width = '20px'>{{($reservable->exemptedvpaa) ? 'yes':'no'}}</td>
			
			<td><form action = '' method = 'post'>
				{!! csrf_field() !!}
				<input type = 'hidden' name = 'reservable_id' value = '{{$reservable->id}}'/>
				@if($reservable->enabled)
				<input type = 'submit' name = 'btnDisable' value = 'disable'/>
				@else
				<input type = 'submit' name = 'btnEnable' value = 'enable'/>
				@endif
				@if(Auth::user()->accountType == "admin")
				<input type = 'submit' name = 'btnEdit' value = 'edit'/>
				<input id = 'btnDelete' onclick="return confirm('Are you sure you want to delete this item?');" type = 'submit' name = 'btnDelete' value = 'delete'/>
				@endif
				
			</form></td>
		</tr>
	@endforeach
 </table>

 <h3 style = "margin-top:30px;">Equipment</h3>
 <table class = "pure-table">
	<thead>
		<tr>
			<th>Serial</th>
			<th>Type</th>
			<th>Name</th>
			<th>Details</th>
			<th>Total quantity</th>
			<th>Enabled</th>
			<th>Action</th>
			
		</tr>
	</thead>
	@foreach($reservableEquipments as $reservable)
		<tr>
			<td>{{$reservable->serial}}</td>
			<td>{{$reservable->type}}</td>
			<td>{{$reservable->venueOrEquipment}}</td>
			<td>{{$reservable->details}}</td>
			<td>{{$reservable->availableQuantity}}</td>
			<td>{{($reservable->enabled) ? 'enabled':'disabled'}}</td>
			
			<td><form action = '' method = 'post'>
				{!! csrf_field() !!}
				<input type = 'hidden' name = 'reservable_id' value = '{{$reservable->id}}'/>
				@if($reservable->enabled)
				<input type = 'submit' name = 'btnDisable' value = 'disable'/>
				@else
				<input type = 'submit' name = 'btnEnable' value = 'enable'/>
				@endif
				@if(Auth::user()->accountType == "admin")
				<input type = 'submit' name = 'btnEdit' value = 'edit'/>
				<input id = 'btnDelete' onclick="return confirm('Are you sure you want to delete this item?');" type = 'submit' name = 'btnDelete' value = 'delete'/>
				@endif
				
			</form></td>
		</tr>
	@endforeach
 </table>


 <h3 style = "margin-top:30px;">Support Services</h3>
 <table class = "pure-table">
	<thead>
		<tr>
			<th>Serial</th>
			<th>Type</th>
			<th>Name</th>
			<th>Details</th>
			<th>Total quantity</th>
			<th>Enabled</th>
			<th>Action</th>
			
		</tr>
	</thead>
	@foreach($reservableSupportService as $reservable)
		<tr>
			<td>{{$reservable->serial}}</td>
			<td>{{$reservable->type}}</td>
			<td>{{$reservable->venueOrEquipment}}</td>
			<td>{{$reservable->details}}</td>
			<td>{{$reservable->availableQuantity}}</td>
			<td>{{($reservable->enabled) ? 'enabled':'disabled'}}</td>
			
			<td><form action = '' method = 'post'>
				{!! csrf_field() !!}
				<input type = 'hidden' name = 'reservable_id' value = '{{$reservable->id}}'/>
				@if($reservable->enabled)
				<input type = 'submit' name = 'btnDisable' value = 'disable'/>
				@else
				<input type = 'submit' name = 'btnEnable' value = 'enable'/>
				@endif
				@if(Auth::user()->accountType == "admin")
				<input type = 'submit' name = 'btnEdit' value = 'edit'/>
				<input id = 'btnDelete' onclick="return confirm('Are you sure you want to delete this item?');" type = 'submit' name = 'btnDelete' value = 'delete'/>
				@endif
				
			</form></td>
		</tr>
	@endforeach
 </table>
@stop
@extends('layout.main4')
@section('content')
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div style = "color:green;">{{ $error }}</div>
  @endforeach
@endif

			<div class="content">
			    <h1>My Account</h1>
				<form class="pure-form pure-form-aligned" method="POST" action="">
					 {!! csrf_field() !!}
				    <fieldset>
				        <div class="pure-control-group">
				            <label for="name">Username</label>
				            <input id="name" required name = "name" type="text" placeholder="Username" value = "{{ Auth::user()->name }}">
				        </div>
			        	<div class="pure-control-group">
				            <label for="name">Email</label>
				            <input id="email" required name = "email" type="text" placeholder="Email" value = "{{Auth::user()->email }}">
				        </div>

				       

				         <div class="pure-control-group">
				            <label for="firstname">First name</label>
				            <input id="firstname" required name = "firstname" type="text" placeholder="first name" value = "{{ Auth::user()->firstname }}">
				        </div>
				         <div class="pure-control-group">
				            <label for="lastname">Last name</label>
				            <input id="lastname" required name = "lastname" type="text" placeholder="first name" value = "{{Auth::user()->lastname}}">
				        </div>
				         <div class="pure-control-group">
				            <label for="middlename">Middle name</label>
				            <input id="middlename"  name = "middlename" type="text" placeholder="Middle  name" value = "{{Auth::user()->middlename}}">
				        </div>
				        <div class="pure-control-group">
				        	@if(Auth::user()->accountType == 'normal')
				            <label for="middlename">Course</label>
				            @endif
				            {!! App\viewStrategy::courses(Auth::user()->course_id) !!}
				        </div>
				         
				        <div class="pure-controls">
				            <button type="submit" name = 'btnSave' class="pure-button pure-button-primary">Save</button>
				        </div>
				    </fieldset>
				</form>
				<!-- @if(Auth::user()->accountType != "normal")
				<h3 style = "margin-top:10px;margin-bottom:10px;">Assigned Department(s) [not yet finished]</h3>
					<ul>
						@foreach(Auth::user()->getDepartments() as $department)
						<li>{{$department->department_desc}}</li>
						@endforeach
						
					</ul>
				 {!! App\viewStrategy::departments(Auth::user()->id) !!}
				 @endif
 -->
			</div><!-- class='content'-->
	
@stop
@extends('layout.main4')
@section('content')
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div style = "color:green;">{{ $error }}</div>
  @endforeach
@endif

			<div class="content">
			    <h1>Edit Department</h1>
				<form class="pure-form pure-form-aligned" method="POST" action="" enctype="multipart/form-data">
					 {!! csrf_field() !!}
				    <fieldset>
				    <input type = 'hidden' name = 'department_id' value = "{{$department->id}}"/>
				        
			        	<div class="pure-control-group">
				            <label >Department Code</label>
				            <input class="pure-input-1-2" required type="text" name = 'departmentcode' placeholder="department code" value = "{{$department->department}}"/>
				        </div>
				       <div class="pure-control-group">
				            <label >Department Name</label>
				            <input class="pure-input-1-2" required type="text" name = 'departmentname' placeholder="department code" value = "{{$department->department_desc}}"/>
				        </div>
				       
				        <div class="pure-controls">
				            <button type="submit" name = "btnSave" class="pure-button pure-button-primary">Save</button>
				            <a href='/systemMaintenance' class="pure-button pure-button-secondary">Back</a>
				        </div>
				    </fieldset>
				</form>

			</div><!-- class='content'-->
	
@stop
@extends('layout.main4')
@section('content')
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div style = "color:green;">{{ $error }}</div>
  @endforeach
@endif

			<div class="content">
			    <h1>Edit Course</h1>
				<form class="pure-form pure-form-aligned" method="POST" action="" enctype="multipart/form-data">
					 {!! csrf_field() !!}
				    <fieldset>
				        
			        	<div class="pure-control-group">
				            <label >Course Code</label>
				            <input class="pure-input-1-2" required type="text" name = 'coursecode' placeholder="course code" value="{{$course->course}}" />
				        </div>
				       <div class="pure-control-group">
				            <label >Course Name</label>
				            <input class="pure-input-1-2" required type="text" name = 'coursename' placeholder="course name"  value="{{$course->course_desc}}"/>
				        </div>
				       <div class="pure-control-group">
				            <label >Department</label>
				            <select class="pure-input-1-2" required name = 'department' placeholder="department" >
			            		<option value = "">--Select Department</option>
			            		@foreach($departments as $department)
			            		<option value="{{$department->id}}" {{($course->department_id == $department->id) ? "selected" : ""}}>{{$department->department}} - {{$department->department_desc}}</option>
			            		@endforeach
				            </select>
				        </div>
				        <div class="pure-controls">
				            <button type="submit" name = "btnSave" class="pure-button pure-button-primary">Save</button>
				            <a href='/systemMaintenance' class="pure-button pure-button-secondary">Back</a>
				        </div>
				    </fieldset>
				</form>

			</div><!-- class='content'-->
	
@stop
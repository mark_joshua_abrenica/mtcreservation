@extends('layout.main4')
@section('content')
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div style = "color:green;">{{ $error }}</div>
  @endforeach
@endif

			<div class="content">
			    <h1>New Department</h1>
				<form class="pure-form pure-form-aligned" method="POST" action="" enctype="multipart/form-data">
					 {!! csrf_field() !!}
				    <fieldset>
				        
			        	<div class="pure-control-group">
				            <label >Department Code</label>
				            <input class="pure-input-1-2" required type="text" name = 'departmentcode' placeholder="department code" />
				        </div>
				       <div class="pure-control-group">
				            <label >Department Name</label>
				            <input class="pure-input-1-2" required type="text" name = 'departmentname' placeholder="department name" />
				        </div>
				       
				        <div class="pure-controls">
				            <button type="submit" name = "btnSave" class="pure-button pure-button-primary">Save</button>
				            <a href='/systemMaintenance' class="pure-button pure-button-secondary">Back</a>
				        </div>
				    </fieldset>
				</form>

			</div><!-- class='content'-->
	
@stop
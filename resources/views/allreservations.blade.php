@extends('layout.main4')

@section('content')
<h1 style = "margin-bottom:20px;">All Reservations</h1>
<form action = '' method = 'get' style = "margin-bottom: 30px;">
<input type = 'date' value = '{{ (isset($_GET["dateStart"])) ? $_GET["dateStart"] : "" }}' name = 'dateStart' />
<input type = 'submit' value = 'filter date'/>
<a style = "margin-left:30px;" href = '/allreservation'>Clear date filter</a>
</form>
 <table class = "pure-table">
	<thead>
		<tr>
			<th>venue</th>
			<th>event</th>
			<th>date / time</th>
			<th>attendants</th>
			<th>Requested by</th>
			<th>Contact number</th>
		</tr>
	</thead>
	<?php $i = 0; ?>
	@foreach($reservations as $reservation)
	
		<tr {!! ($i % 2 == 0) ?  "class='pure-table-odd'" : "" !!}> 
<?php
	$i++;
?>
			<td>{{($reservation->venue()) ? $reservation->venue()->venueOrEquipment : ""}}</td>
			<td>{{$reservation->event}}</td>
			<td>{{date_format(new DateTime($reservation->dateTimeStart), "Y-m-d / h:i:s A")}}</td> 
			<td>{{$reservation->attendants}}</td>
			<td>{{$reservation->user()->firstname . " " . $reservation->user()->lastname}}</td>
			<td>{{$reservation->contactnumber}}</td>
			
		</tr>
	@endforeach
 </table>
@stop
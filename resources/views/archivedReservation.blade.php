@extends('layout.main4')

@section('content')
<h1 style = "margin-bottom:20px;">Archived Reservations</h1>

<form action = '../cleararchivedreservation' method = 'post'>{!! csrf_field() !!}
	<input style = "margin-bottom:20px;" type = 'submit' name = 'btnClearArchivedReservation' value = 'Clear Archived Reservations' onclick="return confirm('Are you sure you want to delete all archived reservation?');"/>
</form>
<form action = '' method = 'get' style = "margin-bottom: 30px;">
<input type = 'date' value = '{{ (isset($_GET["dateStart"])) ? $_GET["dateStart"] : "" }}' name = 'dateStart' />
<input type = 'submit' value = 'filter date'/>
<a style = "margin-left:30px;" href = '/archivedreservations'>Clear date filter</a>
</form>

<a href = "?{{$sortdateeventlink}}">{{$sortdateeventlabel}}</a> &nbsp&nbsp&nbsp
<a href = "?{{$sortdaterequestedlink}}">{{$sortdaterequestedlabel}}</a>
 <table class = "pure-table">
	<thead>
		<tr>
			<th>Requested by</th>
			<th>Date Requested</th>
			<th>purpose</th>
			<th>event</th>
			<th>attendants</th>
			<th>venue</th>
			<th>Equipment / Support Services</th>
			<th>date time start</th>
			<th>date time end</th>
			<th>status</th>
		</tr>
	</thead>
	<?php $i = 1; ?>
	@foreach($reservations as $reservation)
		<tr  {!! ($i % 2 == 0) ?  "class='pure-table-odd'" : "" !!}>
		<?php $i++; ?>
			<td>{{$reservation->user()->firstname . " " . $reservation->user()->lastname}}</td>
			<td>{{$reservation->created_at->format("Y-m-d")}}</td>
			<td>{{$reservation->purpose}}</td>
			<td>{{$reservation->event}}</td>
			<td>{{$reservation->attendants}}</td>
			<td>{{ ($reservation->venue()) ? $reservation->venue()->venueOrEquipment : ""}}</td>
			<td>
				<ul>
				@foreach($reservation->equipments() as $equipment)
				<li>{{$equipment->venueOrEquipment}}</li>
				@endforeach
				</ul>
			</td>
			<td>{{date_format(new DateTime($reservation->dateTimeStart), "Y-m-d / h:i:s A")}}</td>
			<td>{{date_format(new DateTime($reservation->dateTimeEnd), "Y-m-d / h:i:s A")}}</td>
			<td>
			@if($reservation->status != "pending Program Head")
				<ul style ="width:300px; font-size:12px;">
				@foreach($reservation->getApprovallog() as $status)
				<li>{{$status->status}} ({{$status->created_at->format('m-d-Y h:i a')}})</li>
				@endforeach
				</ul>
			@else
			{{$reservation->status}}
			@endif

			</td>
			
		</tr>
	@endforeach
 </table>
@stop
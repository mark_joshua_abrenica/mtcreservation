@extends('layout.main3')
@section('content')
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div style = "color:red;">{{ str_replace("name", "user name", $error) }}</div>
  @endforeach
@endif
			<div class="content">
			    
				<form class="pure-form pure-form-aligned" method="POST" action="/auth/register">
					 {!! csrf_field() !!}
				    <fieldset>
				        <div class="pure-control-group">
				            <label for="name">Account type:</label>
				        	<select name = "accountType">
				        		<option value = "normal">student</option>
				        		<option value = "faculty">faculty</option>
				        	</select>
				        </div>
				        <div class="pure-control-group">
				            <label for="name">Username</label>
				            <input id="name" required name = "name" type="text" placeholder="Username" value = "{{ old('name') }}" />
				        </div>
			        	<div class="pure-control-group">
				            <label for="name">Email</label>
				            <input id="email" required name = "email" type="email" placeholder="Email" value = "{{ old('email') }}" />
				        </div>

				        <div class="pure-control-group">
				            <label for="password">Password</label>
				            <input id="password" required name = "password" type="password" placeholder="Password"/>
				        </div>
				        <div class="pure-control-group">
				            <label for="password">Retype password</label>
				            <input id="password" required name = "password_confirmation" type="password" placeholder="Password"/>
				        </div>


				         <div class="pure-control-group">
				            <label for="firstname">First name</label>
				            <input id="firstname" required name = "firstname" type="text" placeholder="first name" value = "{{ old('firstname') }}"/>
				        </div>
				         <div class="pure-control-group">
				            <label for="lastname">Last name</label>
				            <input id="lastname" required name = "lastname" type="text" placeholder="first name" value = "{{ old('lastname') }}"/>
				        </div>
				         <div class="pure-control-group">
				            <label for="middlename">Middle name</label>
				            <input id="middlename"  name = "middlename" type="text" placeholder="Middle  name" value = "{{ old('middlename') }}"/>
				        </div>
				        <div class="pure-control-group">
				            <label for="middlename">Course</label>
				            {!! App\viewStrategy::courses() !!}
				        </div>
				        <div class="pure-controls">
				            <button type="submit" class="pure-button pure-button-primary">Register</button>
				        </div>
				    </fieldset>
				</form>

			</div><!-- class='content'-->
	
@stop
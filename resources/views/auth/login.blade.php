@extends('layout.main3')
@section('content')
<?php
echo Auth::user();
?>
 
          <div class = "header">
            <h1 style = "color:white; text-align:center;"> <div class="form-top-right">
                                  <i><img width = '100px' src = "{{asset('images/logo.jpg')}}"></img></i>
                                </div>Login</h1>
          </div>
          <div style = "margin-top:30px">
          </div>

			<div class="content">
				<form class="pure-form pure-form-aligned" method="POST" action="/auth/login" style = "text-align:center;">
					 {!! csrf_field() !!}

					  @if (count($errors) > 0)
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li style ="color:red">{{ $error }}</li>
				            @endforeach
				        </ul>
			         @endif
				    <fieldset>
				        <div class="pure-control-group">
				            <label>User name</label>
				            <input id="name" name = "name" type="text" autocomplete="off" required placeholder="Username" value = "{{ old('name') }}">
				        </div>

				        <div class="pure-control-group">
				            <label>Password</label>
				            <input id="password" name = "password" type="password" required placeholder="Password">
				        </div>
				        <div class="pure-control-group">
				            <button type="submit" class="pure-button pure-button-primary">Login</button>
				            <a href = "/auth/register"> register</a><br>
    						<a href = "/allreservation"> View approved reservation</a>
				        </div>
				    </fieldset>
				</form>
			</div><!-- class='content'-->
	
@stop
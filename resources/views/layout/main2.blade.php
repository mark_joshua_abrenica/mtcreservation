
<!DOCTYPE HTML>
<html>
<head>
<title>MTC Reservation Portal</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('js/jquery-1.11.0.min.js')}}"></script>
<!-- Custom Theme files -->
<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Appest Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{asset('js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('js/easing.js')}}"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
<!-- //end-smoth-scrolling -->
<link rel="stylesheet" href="{{asset('css/flexslider.css')}}" type="text/css" media="screen" />
<link rel="stylesheet" href="{{asset('css/pure-min.css')}}">

<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" media="screen" />
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{asset('assets/css/form-elements.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

         <link rel="shortcut icon" href="{{asset('assets/ico/favicon.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('assets/ico/apple-touch-icon-144-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('assets/ico/apple-touch-icon-114-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('assets/ico/apple-touch-icon-72-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" href="{{asset('assets/ico/apple-touch-icon-57-precomposed.png')}}">
<link rel="stylesheet" href="{{asset('css/main.css')}}">
</head>
<body>
	<style scoped>

        .button-success,
        .button-error,
        .button-warning,
        .button-secondary {
            color: white;
            border-radius: 4px;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        }

        .button-success {
            background: rgb(28, 184, 65); /* this is a green */
        }

        .button-error {
            background: rgb(202, 60, 60); /* this is a maroon */
        }

        .button-warning {
            background: rgb(223, 117, 20); /* this is an orange */
        }

        .button-secondary {
            background: rgb(66, 184, 221); /* this is a light blue */
        }

    </style>
<!--header start here-->
<div class="">
  <div class="header">
	<div class="container">
		 <div class="header-main">
				<div class="logo">
					<h1><a href="/">MTC Reservation</a></h1>
					@if(Auth::check())
					<i style = "color:white">logged in as: {{Auth::user()->firstname}} {{Auth::user()->lastname}}</i>
					@endif
				</div>
				<div class="top-nav">
					<span class="menu"> <img src="{{asset('images/icon.png')}}" alt=""/>{!! Auth::user()->getNotifications2() !!}</span>
				 <nav class="cl-effect-16">
					<ul class="res">
					   
					   @if(Auth::check())
					   	@if(Auth::user()->accountType=="normal")
					   	<li><a href="/" data-hover="Home">Home</a></li> 
					   		<li><a  href="../myreservation" >{!! Auth::user()->getNotifications() !!} My reservations</a></li> 
						@else
						<li><a  href="../reservations"  data-hover="Reservations">Reservations</a></li> 						
						<li><a  href="../venuesEquipment"  data-hover="Manage venues/equipment">Manage venues/equipment</a></li> 						
						@endif

						<li><a  href="../myaccount"  data-hover="My Account">My Account</a></li> 

						<li><a  href="../auth/logout"  data-hover="Sign out">Sign out</a></li> 
						@else
						
					   	<li><a href="/" data-hover="Home">Home</a></li> 
						<li><a  href="../auth/register"  data-hover="Register">Register</a></li>
						<li><a  href="../auth/login"  data-hover="Sign in">Sign in</a></li>
						@endif
						<li><a href = "/allreservation" data-hover="View approved reservation">View approved reservation</a><li>
				   </ul>
				 </nav>
					<!-- script-for-menu -->
						 <script>
						   $( "span.menu" ).click(function() {
							 $( "ul.res" ).slideToggle( 300, function() {
							 // Animation complete.
							  });
							 });
						</script>
		        <!-- /script-for-menu -->
				</div>				
		 <div class="clearfix"> </div>
	  </div>
   </div>
</div>

@yield('content')
<!--map start here-->
<!--footer start here-->
<div class="footer">
	<div class="container">
		<div class="footer-main">
			  
		</div>
		<script type="text/javascript">
										$(document).ready(function() {
											/*
											var defaults = {
									  			containerID: 'toTop', // fading element id
												containerHoverID: 'toTopHover', // fading element hover id
												scrollSpeed: 1200,
												easingType: 'linear' 
									 		};
											*/
											
											$().UItoTop({ easingType: 'easeOutQuart' });
											
										});
									</script>
						<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	</div>
</div>
<!--//footer--> 
<script src="{{asset('js/ui.js')}}"></script>
<script src = "{{asset('js/jquery-2.1.4.min.js')}}"></script>


        <!-- Javascript -->
        <script src="{{asset('assets/js/jquery-1.11.1.min.js')}}"></script>
        <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.backstretch.min.js')}}"></script>
        <script src="{{asset('assets/js/retina-1.1.0.min.js')}}"></script>
        <script src="{{asset('assets/js/scripts.js')}}"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->


<script src = "{{asset('js/custom.js')}}"></script>

</body>
</html>
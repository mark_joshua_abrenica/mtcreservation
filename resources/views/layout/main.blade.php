<HTML>
<head>
	<title>
		Our ToDo Application
	</title>
	<link rel = "stylesheet" href = "{{ asset('css/style.css')}}" />
</head>
<body>
<div class = "container">
@yield('content')
</div>
</body>
</HTML
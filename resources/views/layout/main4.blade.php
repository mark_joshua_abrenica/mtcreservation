
<!DOCTYPE HTML>
<html>
<head>
<title>MTC Reservation Portal</title>
<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('js/jquery-1.11.0.min.js')}}"></script>
<!-- Custom Theme files -->
<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Appest Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{asset('js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('js/easing.js')}}"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
<!-- //end-smoth-scrolling -->
<link rel="stylesheet" href="{{asset('css/flexslider.css')}}" type="text/css" media="screen" />
<link rel="stylesheet" href="{{asset('css/pure-min.css')}}">

<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" media="screen" />
</head>
<body>
<!--header start here-->
<div class="">
  <div class="header">
	<div class="container">
		 <div class="header-main">
				<div class="logo">
					<h1><a href="/">MTC Reservation</a></h1>
					@if(Auth::check())
					<i style = "color:white">logged in as: {{Auth::user()->firstname}} {{Auth::user()->lastname}} ({{(Auth::user()->accountType == "normal") ? "student" :  Auth::user()->accountType}})</i>
					@endif
				</div>
				<div class="top-nav">
					<span class="menu"> <img src="{{asset('images/icon.png')}}" alt=""/>{!! (Auth::check()) ? Auth::user()->getNotifications2() : "" !!}</span>
				 <nav class="cl-effect-16">
					<ul class="res">
					   
					 @if(Auth::check())
					   	@if(Auth::user()->accountType=="normal")
					   	<li><a href="/" data-hover="Home">Home</a></li> 
					   	<li><a  href="../myreservation" >{!! Auth::user()->getNotifications() !!} My reservations</a></li> 
						@else
						<li><a  href="../reservations"  data-hover="Reservations">Reservations</a></li> 
						<li><a  href="../archivedreservations"  data-hover="Archived reservations">Archived reservations</a></li> 	
							@if(Auth::user()->accountType=="admin")
							<li><a  href="../venuesEquipment"  data-hover="Reservation maintenance page">Reservation maintenance page</a></li> 
							<li><a  href="../userMaintenance"  data-hover="Accounts maintenance page">Accounts maintenance page</a></li> 						
							<li><a  href="../systemMaintenance"  data-hover="System maintenance page">System maintenance page</a></li> 						
							
							@endif
						@endif

						<li><a  href="../myaccount"  data-hover="My Account">My Account</a></li> 

						<li><a  href="../auth/logout"  data-hover="Sign out">Sign out</a></li> 
					@else
						
					   	<li><a href="/" data-hover="Home">Home</a></li> 
						<li><a  href="../auth/register"  data-hover="Register">Register</a></li>
						<li><a  href="../auth/login"  data-hover="Sign in">Sign in</a></li>
					@endif
						<li><a href = "/allreservation"> View approved reservation</a><li>
				   </ul>
				 </nav>
					<!-- script-for-menu -->
						 <script>
						   $( "span.menu" ).click(function() {
							 $( "ul.res" ).slideToggle( 300, function() {
							 // Animation complete.
							  });
							 });
						</script>
		        <!-- /script-for-menu -->
				</div>				
		 <div class="clearfix"> </div>
	  </div>
   </div>
</div>
<!-- banner Slider starts Here -->
<script src="{{asset('js/responsiveslides.min.js')}}"></script>
<script>
    $(function () {
      $("#slider").responsiveSlides({
      	auto: true,
      	speed: 500,
        namespace: "callbacks",
        pager: false,
        nav:true,
      });
    });
  </script>

<!--gallery start here-->
<div class="gallery" id="gallery">
	<div class="container">
		@if (session('affirm'))
		    <div class="alert alert-success">
		        {{ session('affirm') }}
		    </div>
		@endif
		@if (session('error'))
		    <div class="alert alert-danger">
		        {{ session('error') }}
		    </div>
		@endif
		@yield('content')

	</div>
</div>
<link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
			<script type="text/javascript" src="{{asset('js/nivo-lightbox.min.js')}}"></script>
				<script type="text/javascript">
				$(document).ready(function(){
				    $('#nivo-lightbox-demo a').nivoLightbox({ effect: 'fade' });
				});
				</script>
<div class="footer">
	<div class="container">
		<div class="footer-main">
			  
		</div>
		<script type="text/javascript">
										$(document).ready(function() {
											/*
											var defaults = {
									  			containerID: 'toTop', // fading element id
												containerHoverID: 'toTopHover', // fading element hover id
												scrollSpeed: 1200,
												easingType: 'linear' 
									 		};
											*/
											
											$().UItoTop({ easingType: 'easeOutQuart' });
											
										});
									</script>
						<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	</div>
</div>
<!--//footer--> 

</body>
</html>
@extends('layout.main4')

@section('content')
<h1 style = "margin-bottom:20px;">System Maintenance</h1>
@if(Auth::user()->accountType == "admin")
<a href = "/newdepartment" >Add new Department</a>
@endif
<h3 style = "margin-top:30px;">Departments</h3>


 <table class = "pure-table" style = "margin-bottom:50px;">
	<thead>
		<tr>
			<th>Department Code</th>
			<th>Department Name</th>
			<th>Action</th>
		</tr>
	</thead>
	@foreach($departments as $department)
		<tr>
			<td>{{$department->department}}</td>
			<td>{{$department->department_desc}}</td>
			
			<td><form action = '' method = 'post'>
				{!! csrf_field() !!}
				<input type = 'hidden' name = 'department_id' value = '{{$department->id}}'/>
				
				@if(Auth::user()->accountType == "admin")
				<input type = 'submit' name = 'btnDepartmentEdit' value = 'edit'/>
				<input id = 'btnDepartmentDelete' onclick="return confirm('Are you sure you want to delete this item?');" type = 'submit' name = 'btnDepartmentDelete' value = 'delete'/>
				@endif
				
			</form></td>
		</tr>
	@endforeach
 </table>




 @if(Auth::user()->accountType == "admin")
<a href = "/newcourse" >Add new Course</a>
@endif
<h3 style = "margin-top:30px;">Courses</h3>


 <table class = "pure-table" style = "margin-bottom:50px;">
	<thead>
		<tr>
			<th>Course Code</th>
			<th>Course Name</th>
			<th>Action</th>
		</tr>
	</thead>
	@foreach($courses as $course)
		<tr>
			<td>{{$course->course}}</td>
			<td>{{$course->course_desc}}</td>
			
			<td><form action = '' method = 'post'>
				{!! csrf_field() !!}
				<input type = 'hidden' name = 'course_id' value = '{{$course->id}}'/>
				
				@if(Auth::user()->accountType == "admin")
				<input type = 'submit' name = 'btnCourseEdit' value = 'edit'/>
				<input id = 'btnCourseDelete' onclick="return confirm('Are you sure you want to delete this item?');" type = 'submit' name = 'btnCourseDelete' value = 'delete'/>
				@endif
				
			</form></td>
		</tr>
	@endforeach
 </table>

@stop
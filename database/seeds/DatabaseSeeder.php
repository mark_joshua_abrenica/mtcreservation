<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder
{
    
/**
     * Run the database seeds.
     *
     * @return void
     */
    
public function run()
    {
        
	Model::unguard();
		DB::table('user')->delete();
		// DB::table('reservable')->delete();
		// DB::table('pictures')->delete();
		// DB::table('reservation')->delete();
		$this->call('UserTableSeeder');

 
       
	Model::reguard();
    
}

}

<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    
/**
     * Run the database seeds.
     *
     * @return void
     */
    
public function run(){
	DB::table('user')->delete();
	$users = [
		[
		'name' => 'joshua',
		'firstname' => 'Mark Joshua',
		'lastname' => 'Abrenica',
		'middlename' => 'Rivera',
		'password' => Hash::make('joshua'),
		'email' => 'abrenicamarkjoshua@gmail.com'
		]
	];
	DB::table('user')->insert($users);
}//end of run
}//end of class

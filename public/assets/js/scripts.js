
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    
    $('#top-navbar-1').on('shown.bs.collapse', function(){
    	$.backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
    	$.backstretch("resize");
    });
    
    /*
        Form
    */
    $('.registration-form fieldset:first-child').fadeIn('slow');
    
    $('.registration-form input[type="text"], .registration-form input[type="password"], .registration-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    // next step
    $('.registration-form .btn-next').on('click', function() {
     
     
    	var parent_fieldset = $(this).parents('fieldset');
    	var next_step = true;
        
    	parent_fieldset.find('input[type="radio"],input[type="check"],input[type="time"],input[type="text"],input[type="number"], input[type="password"],input[type="file"],input[type="date"], select, textarea').each(function() {
    		
            if( $(this).val() == "" ) {
                if($(this).is("select")){
                    $(this).addClass('input-error');
                    next_step = false;
                }
                if($(this).prop("required")){
        			$(this).addClass('input-error');
        			next_step = false;
                }
                

    		}
    		else {
    			$(this).removeClass('input-error');
    		}


    	});

        if($("#dateStart").val() > $("#dateEnd").val()){
                next_step = false;
                alert("date start must be before date end");
            } 
            if($("#dateStart").val() == $("#dateEnd").val()){
                 
                if($("#timeStart").val() > $("#timeEnd").val()){
                    next_step = false;
                   alert("time start must be before time end"); 
                }
                if($("#timeStart").val() == $("#timeEnd").val()){
                    next_step = false;
                   alert("same date time start and date time end is not allowed"); 
                }
                
            }
    	
    	if( next_step ) {
    		parent_fieldset.fadeOut(400, function() {
	    		$(this).next().fadeIn();
                
                $("html, body").animate({ scrollTop: 0 }, "slow");
	    	});
    	}
    	
    });
    
    // previous step
    $('.registration-form .btn-previous').on('click', function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).prev().fadeIn();
    	});
    });
    
    // submit
    $('.registration-form').on('submit', function(e) {
        
    	$(this).find('input[type="text"], input[type="email"],input[type="file"],input[type="date"], select').each(function() {
    		if( $(this).val() == "" ) {
                 if($(this).is("select")){
                    e.preventDefault();
                    $(this).addClass('input-error');
                }
                if($(this).prop("required")){
                   e.preventDefault();
                $(this).addClass('input-error');
                }
    			
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });
    
    
});

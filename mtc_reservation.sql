-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 15, 2016 at 07:15 PM
-- Server version: 5.5.45-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mtcroomr_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `approvallog`
--

CREATE TABLE IF NOT EXISTS `approvallog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_id` (`reservation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `approvallog`
--

INSERT INTO `approvallog` (`id`, `reservation_id`, `status`, `created_at`, `updated_at`) VALUES
(52, 154, 'Endorsed by Program Head', '2016-03-08 10:12:15', '2016-03-08 10:12:15'),
(63, 153, 'Endorsed by Program Head', '2016-03-08 12:09:20', '2016-03-08 12:09:20'),
(69, 159, 'Endorsed by Program Head', '2016-03-11 15:21:06', '2016-03-11 15:21:06'),
(70, 159, 'Endorsed by Dean', '2016-03-11 15:21:35', '2016-03-11 15:21:35'),
(71, 159, 'Endorsed by VPAA', '2016-03-11 15:21:54', '2016-03-11 15:21:54'),
(72, 159, 'Approved by admin', '2016-03-11 15:22:07', '2016-03-11 15:22:07');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `department_id` int(11) NOT NULL,
  `course_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course`, `created_at`, `updated_at`, `department_id`, `course_desc`) VALUES
(1, 'BSIT', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 'Bachelor of Science in Information Technology'),
(2, 'BSHNCA', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 'Bachelor o Sscience in Holistic  Nutrition with Culinary Arts'),
(3, 'BSP', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 'Bachelor  of Science in Psychology'),
(4, 'BSA', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Bachelor of Science in Accountancy'),
(5, 'BSE', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Bachelor of Science in Entrepreneurship'),
(6, 'BSHRM', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Bachelor of Science in Hotel and Restaurant Management'),
(7, 'BSBA', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Bachelor of Science in Business Administration'),
(8, 'BSN', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 'Bachelor of Science in Nursing');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(255) NOT NULL,
  `department_desc` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department`, `department_desc`, `created_at`, `updated_at`) VALUES
(1, 'CAM', 'College of Accountancy and Management', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'CAS', 'College of Arts and Sciences', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'CON', 'College of Nursing', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE IF NOT EXISTS `pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservable_id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `process`
--

CREATE TABLE IF NOT EXISTS `process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `process_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `process`
--

INSERT INTO `process` (`id`, `process_desc`) VALUES
(1, 'student reservation with venue or equipment'),
(2, 'student reservation without venue or equipment (no VPAA approval)');

-- --------------------------------------------------------

--
-- Table structure for table `reservable`
--

CREATE TABLE IF NOT EXISTS `reservable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `venueOrEquipment` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `serial` varchar(255) DEFAULT NULL,
  `availableQuantity` int(11) NOT NULL DEFAULT '1',
  `exemptedvpaa` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `reservable`
--

INSERT INTO `reservable` (`id`, `type`, `venueOrEquipment`, `details`, `picture`, `enabled`, `created_at`, `updated_at`, `serial`, `availableQuantity`, `exemptedvpaa`) VALUES
(1, 'equipment', 'Tytana Podium', 'Manila Tytana Colleges podium', 'images/image001.jpg', 0, '0000-00-00 00:00:00', '2016-03-10 22:01:04', 'MTC PODIUM', 3, 0),
(2, 'equipment', 'Tytana Flag', 'Manila Tytana Colleges flag', 'images/g5.jpg', 0, '0000-00-00 00:00:00', '2016-03-10 22:01:11', 'MTC FLAG', 3, 0),
(3, 'venue', 'Room 220', '2nd floor', 'images/g2.jpg', 1, '0000-00-00 00:00:00', '2016-03-11 07:23:57', 'RM 220', 30, 1),
(4, 'venue', 'Gymnasium', '6th floor', 'images/g3.jpg', 1, '0000-00-00 00:00:00', '2016-02-21 12:57:05', 'GYM', 30, 0),
(5, 'equipment', 'Philippine Flag', 'Philippine flag', 'images/g4.jpg', 1, '0000-00-00 00:00:00', '2016-02-21 13:02:47', 'PH FLAG', 3, 0),
(6, 'venue', 'Auditorium', '6th floor', 'images/g6.jpg', 1, '0000-00-00 00:00:00', '2016-02-21 13:01:08', 'AUDITORIUM', 30, 0),
(8, 'venue', 'Atrium', 'Ground floor', 'images/Penguins.jpg', 1, '2015-12-07 03:31:06', '2016-02-21 13:01:31', 'ATRIUM', 30, 0),
(12, 'equipment', 'Podium', 'White podium', 'images/f3.jpg', 1, '2015-12-15 03:27:12', '2016-02-21 13:13:31', 'PODIUM', 3, 0),
(13, 'Support Services', 'Electronic registration (ID Tapping)', 'ID Tapping for attendance purposes', '', 1, '0000-00-00 00:00:00', '2016-03-10 22:07:44', 'ID TAP', 3, 1),
(19, 'venue', 'Room 221', '2nd floor', '', 1, '2016-02-21 12:56:18', '2016-02-21 12:56:18', 'RM 221', 30, 1),
(21, 'equipment', 'Microphone Stand', 'Adjustable microphone stand', '', 1, '2016-03-10 22:00:50', '2016-03-10 22:00:50', 'MIC STAND', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attendants` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dateTimeStart` datetime NOT NULL,
  `dateTimeEnd` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `purpose` text NOT NULL,
  `venue` int(11) NOT NULL,
  `event` varchar(255) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `dateStart` date NOT NULL,
  `remarks` text NOT NULL,
  `contactnumber` varchar(255) NOT NULL,
  `process_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `process_id` (`process_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=164 ;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id`, `attendants`, `user_id`, `dateTimeStart`, `dateTimeEnd`, `status`, `created_at`, `updated_at`, `purpose`, `venue`, `event`, `notify`, `dateStart`, `remarks`, `contactnumber`, `process_id`) VALUES
(105, 100, 17, '2016-12-21 10:00:00', '2016-12-21 11:00:00', 'pending Program Head', '2016-02-21 14:11:32', '2016-02-21 14:11:32', 'vpaa', 3, 'vpaa', 0, '2016-12-21', '', '111', 1),
(107, 4, 17, '0004-04-04 16:44:00', '0004-04-04 16:44:00', 'pending Program Head', '2016-02-22 12:05:57', '2016-02-22 12:05:57', 'test', 19, 'test', 0, '0004-04-04', '', '234234', 1),
(108, 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'pending Program Head', '2016-02-22 13:28:27', '2016-02-22 13:28:27', 'jdjdjd', 0, 'phone', 0, '0000-00-00', '', '65688', 2),
(109, 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'pending Program Head', '2016-02-22 13:28:37', '2016-02-22 13:28:37', 'jdjdjd', 0, 'phone', 0, '0000-00-00', '', '65688', 2),
(110, 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'pending Program Head', '2016-02-22 13:33:48', '2016-02-22 13:33:48', 'jdjdjd', 0, 'phone3', 0, '0000-00-00', '', '65688', 2),
(111, 2, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'pending Program Head', '2016-02-22 13:51:20', '2016-02-22 13:51:20', 'phone', 0, 'phone2', 0, '0000-00-00', '', '88899', 2),
(112, 8, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'pending Program Head', '2016-02-22 14:00:35', '2016-02-22 14:00:35', 'test', 3, 'phone4', 0, '0000-00-00', '', '049768668', 2),
(113, 8, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'pending Program Head', '2016-02-22 14:13:47', '2016-02-22 14:13:47', 'test', 3, 'phone4', 0, '0000-00-00', '', '049768668', 2),
(114, 0, 17, '2016-02-23 00:00:00', '2016-02-23 00:00:00', 'pending Program Head', '2016-02-22 15:27:30', '2016-02-22 15:27:30', 'hello', 8, 'phone5', 0, '2016-02-23', '', '85559', 2),
(115, 0, 17, '2016-02-23 00:00:00', '2016-02-23 00:00:00', 'pending Program Head', '2016-02-22 15:27:43', '2016-02-22 15:27:43', 'hello', 8, 'phone5', 0, '2016-02-23', '', '85559', 2),
(116, 2, 17, '2016-02-23 00:00:00', '2016-02-23 00:00:00', 'pending Program Head', '2016-02-22 15:28:10', '2016-02-22 15:28:10', 'hello', 8, 'phone5', 0, '2016-02-23', '', '85559', 2),
(117, 100, 17, '2016-02-24 03:00:00', '2016-02-24 03:00:00', 'pending Program Head', '2016-02-22 15:50:50', '2016-02-22 15:50:50', '', 3, 'phone7', 0, '2016-02-24', '', '', 1),
(118, 0, 17, '2016-02-24 09:54:00', '2016-02-24 09:54:00', 'pending Program Head', '2016-02-22 15:55:30', '2016-02-22 15:55:30', '', 3, 'phone8', 0, '2016-02-24', '', '', 1),
(119, 100, 17, '2016-02-24 09:54:00', '2016-02-24 09:54:00', 'pending Program Head', '2016-02-22 15:56:31', '2016-02-22 15:56:31', 'b', 3, 'phone8', 0, '2016-02-24', '', '049799*', 1),
(120, 0, 17, '2016-02-25 07:57:00', '2016-02-25 07:57:00', 'pending Program Head', '2016-02-22 15:58:28', '2016-02-22 15:58:28', 'help', 3, 'phone9', 0, '2016-02-25', '', '8#96', 1),
(121, 158, 17, '2016-02-25 08:03:00', '2016-02-25 08:03:00', 'pending Program Head', '2016-02-22 16:03:56', '2016-02-22 16:03:56', 'b', 8, 'phone10', 0, '2016-02-25', '', '79797', 1),
(122, 0, 17, '2016-02-25 01:18:00', '2016-02-25 01:18:00', 'pending Program Head', '2016-02-22 16:19:41', '2016-02-22 16:19:41', '2l', 3, 'phone11', 0, '2016-02-25', '', '9898988', 2),
(123, 0, 17, '2016-02-25 01:18:00', '2016-02-25 01:18:00', 'pending Program Head', '2016-02-22 16:19:47', '2016-02-22 16:19:47', '2l', 3, 'phone11', 0, '2016-02-25', '', '9898988', 2),
(124, 100, 17, '2016-02-23 08:00:00', '2016-02-23 08:00:00', 'pending Program Head', '2016-02-22 16:33:21', '2016-02-22 16:33:21', 'hello', 6, 'phone12', 0, '2016-02-23', '', '09518639393', 1),
(125, 8, 17, '2016-02-23 08:42:00', '2016-02-23 08:42:00', 'pending Program Head', '2016-02-23 00:43:16', '2016-02-23 00:43:16', 'test', 8, 'phone14', 0, '2016-02-23', '', '05554', 1),
(126, 100, 17, '2016-02-23 11:57:00', '2016-02-23 11:57:00', 'pending Program Head', '2016-02-23 00:57:53', '2016-02-23 00:57:53', 'test', 3, 'phone15', 0, '2016-02-23', '', '12334558', 2),
(127, 100, 17, '2016-02-23 09:00:00', '2016-02-23 09:00:00', 'pending Program Head', '2016-02-23 01:00:09', '2016-02-23 01:00:09', 'testing', 6, 'phone16', 0, '2016-02-23', '', '055889', 2),
(129, 100, 17, '2016-02-23 09:05:00', '2016-02-23 10:05:00', 'pending Program Head', '2016-02-23 01:05:36', '2016-02-23 01:05:36', 'test', 3, 'phone19', 0, '2016-02-23', '', '7808', 2),
(131, 100, 17, '2016-02-23 11:28:00', '2016-02-23 12:38:00', 'pending Program Head', '2016-02-23 01:39:21', '2016-02-23 01:39:21', 'test', 4, 'phone20', 0, '2016-02-23', '', '097664', 1),
(132, 100, 17, '2016-02-23 11:28:00', '2016-02-23 12:38:00', 'pending Program Head', '2016-02-23 01:39:21', '2016-02-23 01:39:22', 'test', 4, 'phone20', 0, '2016-02-23', '', '097664', 1),
(133, 100, 17, '2016-02-23 09:51:00', '2016-02-23 10:51:00', 'pending Program Head', '2016-02-23 03:53:14', '2016-02-23 03:53:14', 'help', 6, 'phone21', 0, '2016-02-23', '', '8936', 1),
(134, 100, 17, '2016-02-23 09:51:00', '2016-02-23 10:51:00', 'pending Program Head', '2016-02-23 03:53:31', '2016-02-23 03:53:31', 'help', 6, 'phone21', 0, '2016-02-23', '', '8936', 1),
(135, 100, 17, '2016-02-23 09:51:00', '2016-02-23 10:51:00', 'pending Program Head', '2016-02-23 03:53:32', '2016-02-23 03:53:32', 'help', 6, 'phone21', 0, '2016-02-23', '', '8936', 1),
(136, 100, 17, '2016-02-23 09:51:00', '2016-02-23 10:51:00', 'pending Program Head', '2016-02-23 03:53:32', '2016-02-23 03:53:32', 'help', 6, 'phone21', 0, '2016-02-23', '', '8936', 1),
(137, 100, 17, '2016-02-23 09:51:00', '2016-02-23 10:51:00', 'pending Program Head', '2016-02-23 03:53:33', '2016-02-23 03:53:33', 'help', 6, 'phone21', 0, '2016-02-23', '', '8936', 1),
(151, 100, 17, '2016-03-10 10:00:00', '2016-03-10 11:00:00', 'pending Program Head', '2016-03-02 10:25:16', '2016-03-02 10:25:16', 'asd', 4, 'asd', 0, '2016-03-10', '', '111', 2),
(153, 100, 17, '2016-03-10 10:00:00', '2016-03-10 11:00:00', 'pending Dean', '2016-03-08 10:04:53', '2016-03-11 08:53:10', 'asda', 19, 'hahaha', 0, '2016-03-10', '', '111', 2),
(154, 1000, 17, '2016-03-10 10:00:00', '2016-03-10 11:00:00', 'pending Dean', '2016-03-08 10:11:56', '2016-03-11 08:53:12', 'asdsa', 3, 'hahahaha', 0, '2016-03-10', '', '111', 1),
(159, 21, 17, '2016-03-18 12:00:00', '2016-03-18 14:00:00', 'approved', '2016-03-11 15:20:43', '2016-03-11 15:22:07', 'Requirement for IT Issues and Seminar', 3, 'Habits of an Effective Programmer', 1, '2016-03-18', '', '09051682168', 1),
(160, 30, 17, '2016-03-16 18:06:00', '2016-03-16 18:06:00', 'pending Program Head', '2016-03-15 18:06:51', '2016-03-15 18:06:51', 'ggbk', 8, 'test', 0, '2016-03-16', '', '55860', 1),
(161, 25, 17, '2016-03-17 08:11:00', '2016-03-17 11:11:00', 'pending Program Head', '2016-03-16 08:11:45', '2016-03-16 08:11:45', 'test', 8, 'test', 0, '2016-03-17', '', '67949#767', 2),
(162, 25, 17, '2016-03-17 08:11:00', '2016-03-17 11:11:00', 'pending Program Head', '2016-03-16 08:11:45', '2016-03-16 08:11:45', 'test', 8, 'test', 0, '2016-03-17', '', '67949#767', 2),
(163, 25, 17, '2016-03-17 08:11:00', '2016-03-17 11:11:00', 'pending Program Head', '2016-03-16 08:11:46', '2016-03-16 08:11:46', 'test', 8, 'test', 0, '2016-03-17', '', '67949#767', 2);

-- --------------------------------------------------------

--
-- Table structure for table `reserved`
--

CREATE TABLE IF NOT EXISTS `reserved` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) NOT NULL,
  `reservable_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `dateTimeStart` datetime NOT NULL,
  `dateTimeEnd` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_id` (`reservation_id`),
  KEY `reservable_id` (`reservable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=175 ;

--
-- Dumping data for table `reserved`
--

INSERT INTO `reserved` (`id`, `reservation_id`, `reservable_id`, `quantity`, `dateTimeStart`, `dateTimeEnd`, `created_at`, `updated_at`, `status`) VALUES
(160, 154, 1, 3, '2016-03-10 10:00:00', '2016-03-10 11:00:00', '2016-03-08 10:11:56', '2016-03-08 10:12:28', 'pending admin'),
(161, 154, 2, 3, '2016-03-10 10:00:00', '2016-03-10 11:00:00', '2016-03-08 10:11:56', '2016-03-08 10:12:28', 'pending admin'),
(162, 154, 12, 3, '2016-03-10 10:00:00', '2016-03-10 11:00:00', '2016-03-08 10:11:56', '2016-03-08 10:12:28', 'pending admin'),
(171, 159, 5, 1, '2016-03-18 12:00:00', '2016-03-18 14:00:00', '2016-03-11 15:20:43', '2016-03-11 15:22:07', 'approved'),
(172, 159, 12, 1, '2016-03-18 12:00:00', '2016-03-18 14:00:00', '2016-03-11 15:20:43', '2016-03-11 15:22:07', 'approved'),
(173, 159, 13, 1, '2016-03-18 12:00:00', '2016-03-18 14:00:00', '2016-03-11 15:20:43', '2016-03-11 15:22:07', 'approved'),
(174, 160, 5, 1, '2016-03-16 18:06:00', '2016-03-16 18:06:00', '2016-03-15 18:06:51', '2016-03-15 18:06:51', 'pending Program Head');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `remember_token` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `accountType` varchar(255) NOT NULL DEFAULT 'normal',
  `course_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `course_id` (`course_id`),
  KEY `department_id` (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `password`, `email`, `firstname`, `lastname`, `middlename`, `remember_token`, `created_at`, `updated_at`, `accountType`, `course_id`, `department_id`, `active`) VALUES
(7, 'admin', '$2y$10$9RS1ktuSOu9KBYdooyWMCe3mNw22XkMYZEs6Er0RVSm9MFFMSypqC', 'ratan@mtc.edu.ph', 'Ronaldo', 'Tan', 'A', 'tH2XoRLUd51ue24Eg5Xxnu8iedgo3u9t4KtMbOm51b80rj7owHt2M2u6Opea', '2015-12-03 03:47:05', '2016-03-11 15:22:22', 'admin', NULL, NULL, 1),
(10, 'IT Head', '$2y$10$9RS1ktuSOu9KBYdooyWMCe3mNw22XkMYZEs6Er0RVSm9MFFMSypqC', 'rjfamini@mtc.edu.ph', 'Rita', 'Famini', 'J', 'Vi37uHYc0HHmv15JjzEIRL4XDlugUaqjlZFqnAi1mhxdcr60mgTc4fMcLlhD', '0000-00-00 00:00:00', '2016-03-11 15:21:12', 'Program Head', 1, 2, 1),
(11, 'CAS Dean', '$2y$10$9RS1ktuSOu9KBYdooyWMCe3mNw22XkMYZEs6Er0RVSm9MFFMSypqC', 'ldtamboon@mtc.edu.ph', 'Lorna', 'Tamboong', 'D', 'dapRPAILzPUHFQQ0q3zf0ipQg2zOWLuKpHNM2miNgm6ohbyHapPlJsinmwQX', '0000-00-00 00:00:00', '2016-03-11 15:21:37', 'Dean', NULL, 2, 1),
(12, 'VPAA', '$2y$10$9RS1ktuSOu9KBYdooyWMCe3mNw22XkMYZEs6Er0RVSm9MFFMSypqC', 'lcreynoso@mtc.edu.ph', 'Lino', 'Reynoso', 'C', 'pj0K3tpOvLdZi0mwBE0cpdjyJGn6BM47HDP387SKzeyBhaCOJZCWl2lqVxYm', '0000-00-00 00:00:00', '2016-03-11 15:21:57', 'VPAA', NULL, NULL, 1),
(17, '2012100092', '$2y$10$7GEunEUxAKGjbszVCLzve.u5NIj39YOf03dE308lm5CV/AXuOsxK6', 'mmendoza_13@mtc.edu.ph', 'Mardonking John', 'Mendoza', 'Barbaza', '5HWFxKLURmq5gvZaXCeTOhxhfA1JVjU68b2X0R88JOB8ciRR9ENbvEEuwDrP', '2016-02-10 05:28:24', '2016-03-11 15:20:48', 'normal', 1, 2, 1),
(18, 'nursing', '$2y$10$I/O5jMylN790IQSsz/zgKeiadfHqbthtwPQzdd.meRRifLNitUL3S', 'nursing@mtc.edu.ph', 'nursing', 'nursing', 'nursing', 'mPWmHIp6r0fkLGbxuiRFkS8k1ATqV93JaY9yRCKoPvZGHzO8AeGtU1RGyAZZ', '2016-02-21 13:16:18', '2016-02-21 13:41:09', 'normal', 8, NULL, 0),
(19, 'Nursing Head', '$2y$10$9RS1ktuSOu9KBYdooyWMCe3mNw22XkMYZEs6Er0RVSm9MFFMSypqC', 'nhead@mtc.edu.ph', 'nursing head', 'nursing head', 'nursing head', 'WCyj2HcHuq7k6rvbrCrFIRDz21XFTDls3LLuvoUM7Sx44dQ3QjPm4sy38kvJ', '2016-02-21 13:18:22', '2016-03-09 16:43:01', 'Program Head', 8, 3, 1),
(20, 'CON dean', '$2y$10$B6M1iZ8Za7EiKy/MlZiKyOgOuB8rZFiAbRUbnNu7xd.yZNideq7Fe', 'condean@mtc.edu.ph', 'CON dean', 'CON dean', 'CON dean', 'NWvme5w3aqRUxYqLjqueBMeZWEj9GizSqtAb0eNvGf4xOQGl1nv8EcbX0qqA', '2016-02-21 13:20:16', '2016-02-21 13:42:41', 'Dean', NULL, NULL, 1),
(21, 'entrep', '$2y$10$w8tv4DFjMeGNtj5WrAhwce8MiZGQlttjIsPzIA/bIgsGN0PT1N3DS', 'entrep@mtc.edu.ph', 'entrep', 'entrep', 'entrep', '77NyOwL7ZaLGik2YeiLw3Mt3zAg54ZXu1RpF0I05szgJs35n506W2WgIoiuN', '2016-02-21 13:21:06', '2016-02-21 13:21:17', 'normal', 5, NULL, 0),
(22, 'Entrep Head', '$2y$10$F9Dv89gOmxXp1lXESe.Rf..uv4CIiilHateBO3pX7znsBIdxz7KE.', 'eheads@mtc.edu.ph', 'entrep head', 'entrep head', 'entrep head', 'XWZNH44AKZARN2SbXdgqoXr3EGJXqLqus4Ke9ttMYyjH6x2nz75kgY1bleqK', '2016-02-21 13:21:46', '2016-02-21 13:21:58', 'Program Head', NULL, 1, 1),
(23, 'CAM dean', '$2y$10$aD/UyJbG1A6TZ3N/YT9G2..pqS4gP8/3LQENLJeTtrTK.o74NOLsO', 'camdean@mtc.edu.ph', 'CAM dean', 'CAM dean', 'CAM dean', 'Qcpkq1C0pRFuDBTTXxltmzC6fCYp67JqrIrcJlWabS2bzD9SmqE1GBz6IOi0', '2016-02-21 13:22:20', '2016-02-21 13:22:23', 'Dean', NULL, NULL, 1),
(24, '2012100157', '$2y$10$cGd9/uAjIwOnja3lSXpm1ufRAnrYk/Hv7e5kVZfE1TLtmyePl3DqC', 'non_nonato@yahoo.com', 'Neil', 'Nonato', '', 'j1SJLt4fciS8wdl2E3NZlqk12WIqnX56XiFDkDbVtsNSKAeF4KKzZ13eeDV3', '2016-02-22 06:21:43', '2016-03-08 10:04:14', 'normal', 1, NULL, 1),
(25, '2012100169', '$2y$10$hc3gOJCVrkeARWlvrL01XevgnAc5B829nmsqxOjcfmHQLP/fFr0Au', 'alxfrnndz002@gmail.com', 'Alexander', 'Fernandez III', 'Cabarles', 'RSQIsC7IPH6fZrxrYIT00M0hAtZQQELkWdFCB28DiWWvoalg0QEiC4zCwb5j', '2016-02-23 01:02:07', '2016-02-23 05:18:40', 'normal', 1, NULL, 0),
(27, '2012100156', '$2y$10$LpImU3KYJlTPlB7YgBf7veWV1Z4Dhsz8IUUl/cCK0iUnVo9WSPp7e', 'lairdvidal@gmail.com', 'Laird Duzel', 'Vidal', 'Oro', '', '2016-02-23 02:38:00', '2016-02-23 02:38:00', 'normal', 1, NULL, 0),
(28, 'lairdvidal', '$2y$10$Aul/6QumxWUre5AT5dmtP.2aGNAZcb1dWe/i6OogixgLa4Ru.1zm6', 'duzeloro@gmail.com', 'Laird Duzel', 'Vidal', 'Oro', 'UABCefhPmJbGUVD9dxkwQ8bhmJSACiXWohqSiBwD7NOO7EHKoqq5Fl8oFH9N', '2016-02-23 15:06:10', '2016-02-23 15:08:09', 'normal', 1, NULL, 0),
(31, 'testmj', '$2y$10$lIRQBRXsBPGMkWU5EYpGi.8b.WlmDLQi1vugRXe2Mc85PCO0Fip6q', 'testmj@abrenica.com', 'testmj', 'testmj', 'testmj', 'XvBu5VP6iFhXSVshXOBhpePf7vgT1h18cDkw2ouKE5cBKXXeI4XLd0O3nded', '2016-03-08 09:15:52', '2016-03-08 09:24:29', 'normal', 1, NULL, 0),
(32, 'testmj2', '$2y$10$mpXKvhEHcFNp567A7QCuO.8DRpIs7C1mpKYaRy6E3.ZP3RXaguSHC', 'testmj2@testmj2.com', 'testmj', 'testmj', 'testmj', 'clYckzs2Z2jpzvqLeDJlJv8B1CMcmEB0YSi0tfmCcCDOPKDZ7lC8a4NTSENl', '2016-03-08 09:24:55', '2016-03-08 09:24:55', 'normal', 1, NULL, 0),
(33, '20121000924', '$2y$10$pKstDBTYNzARfUcISaSSq.eCEUIEfAV6.wYdBmEOz7QtLyiqWxyIe', 'asda@email.com.ph', 'asda', 'asd', 'asda', 'bjyZuctaCkMjlzT1iIbT5Lb182xADmkB2glw0VVZF126mNvcBxqTxfosctHS', '2016-03-08 09:52:37', '2016-03-08 09:52:37', 'normal', 1, NULL, 0),
(38, 'username', '$2y$10$poZ2zhrmA0qxhx8xztoeNef.LQdL2SsvGwS.Mid5qbyyqcsxXNpxe', 'email@gmaill.com', 'user', 'name', 'middle', 'gceAzT1MX8OG6FlQtZuf99oIjv9V1tC137KSurga8Z96yW2qq3TwMeW3pOTi', '2016-03-08 13:14:24', '2016-03-08 13:14:24', 'normal', 1, NULL, 0),
(39, 'teststudent1', '$2y$10$saDC0z4.p27CNqaYSMm3huF9A0qFuuyeQa0r6FzUeXurdFQVThDKO', 'test@teststudent.com', 'test', 'test', 'test', 'eGvHiIH1AuIOUwKQNTpC83JKgKXLY4j6D5nhaPQAne4pXOjzGTiZMAXAEDFG', '2016-03-10 10:18:53', '2016-03-10 10:19:09', 'normal', 1, NULL, 0),
(40, 'testfaculty', '$2y$10$yOgB.VDnJiwj9BZX6MM0OuO.tBPvcwECK86.IiEXSf2MJAqCkj5sK', 'testfaculty@testfaculty.com', 'testfaculty', 'testfaculty', 'testfaculty', 'E6YRGmAKjUDKp1SxotAmQAq49vFiPywROreFwWSVZzt7T3ln80HydG8l4GUo', '2016-03-10 10:22:15', '2016-03-10 12:54:03', 'faculty', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `userdepartment`
--

CREATE TABLE IF NOT EXISTS `userdepartment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `department_id` (`department_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `userdepartment`
--

INSERT INTO `userdepartment` (`id`, `user_id`, `department_id`, `course_id`) VALUES
(3, 10, 2, 1),
(4, 11, 2, NULL),
(6, 19, 3, 8),
(7, 20, 3, NULL),
(8, 23, 1, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `approvallog`
--
ALTER TABLE `approvallog`
  ADD CONSTRAINT `approvallog_ibfk_1` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`);

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`process_id`) REFERENCES `process` (`id`),
  ADD CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reserved`
--
ALTER TABLE `reserved`
  ADD CONSTRAINT `reserved_ibfk_1` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reserved_ibfk_2` FOREIGN KEY (`reservable_id`) REFERENCES `reservable` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`);

--
-- Constraints for table `userdepartment`
--
ALTER TABLE `userdepartment`
  ADD CONSTRAINT `userdepartment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `userdepartment_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `userdepartment_ibfk_3` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
